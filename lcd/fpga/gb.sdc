derive_pll_clocks -create_base_clocks -use_net_name
derive_clock_uncertainty
create_clock -name "wb_clock_clk" -period "10MHz" [get_ports wb_clock_clk] -add
create_generated_clock -name "rdpscl" -divide_by 2 -source [get_pins pll50to20_inst|altpll_component|auto_generated|pll1|clk[0]] -master_clock {*pll50to20*} [get_registers lcd_bridge_0|frame_processing_inst|frame_buf_ctrl_inst|rd_pscl]
set_clock_groups -asynchronous -group {wb_clock_clk} -group {*pll50to20* clk_clk rdpscl}

set_false_path -from [get_registers *user_itf*|r*] -to [get_clocks rdpscl]
set_false_path -from [get_registers lcd_bridge_top:lcd_bridge_0|frame_processing:frame_processing_inst|frame_buf_ctrl:frame_buf_ctrl_inst|rdframe_en] -to [get_clocks rdpscl]