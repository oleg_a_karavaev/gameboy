/*
 * DMGLCD.h
 *
 * Created: 3/30/2018 2:18:53 PM
 *  Author: Bugs Bunny
 */ 

#ifndef DMGLCD_H_
#define DMGLCD_H_

#include "baseinclude.h"

#ifndef F_CPU
#define F_CPU 8000000UL
#endif

// LCD module CS + DC + RST

// Colors
#define WHITE	0xFFFF
#define BLACK	0x0000
#define BLUE	0x001F
#define RED		0xF800
#define MAGENTA	0xF81F
#define GREEN	0x07E0
#define CYAN	0x7FFF
#define YELLOW	0xFFE0
#define BROWN	0xBC40
#define GRAY	0x8430

// #include <util/delay.h>
#include "SPI.h"

void DMGLCD_CS_HI();
void DMGLCD_CS_LO();
void DMGLCD_DC_DAT();
void DMGLCD_DC_CMD();
void DMGLCD_RST_HI();
void DMGLCD_RST_LO();
void DMGLCDInit(void);
void LCDInit(void);
void DMGLCDWriteCommand(alt_u8 cmd);
void DMGLCDWriteData(alt_u8 data);
void DMGLCDTestColor();

#endif /* DMGLCD_H_ */
