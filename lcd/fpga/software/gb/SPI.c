/*
 * SPI.c
 *
 *  Created on: 09 ����. 2018 �.
 *      Author: Dell
 */

#include "SPI.h"

static int delay;

void SPIInit(void) {
	//IOWR_ALTERA_AVALON_PIO_DIRECTION(SCK_BASE, 1);
	IOWR_ALTERA_AVALON_PIO_DATA(SCK_BASE, 1);
//	IO_WR(SS_BASE, DIR_OFFS, 1);
//	IO_WR(SS_BASE, DATA_OFFS, 1);
	//IOWR_ALTERA_AVALON_PIO_DIRECTION(MOSI_BASE, 1);
	IOWR_ALTERA_AVALON_PIO_DATA(MOSI_BASE, 1);
	//IOWR_ALTERA_AVALON_PIO_DIRECTION(MISO_BASE, 0);
	delay = SPI_LEN;
}

static void waitck(int val){
	int cnt = 0;
	while (cnt < val) {
		cnt = cnt + 1;
	}
}

alt_u8 SPITransaction(alt_u8 data) {
	alt_u8 tmp = 0;
//	IO_WR(SS_BASE, DATA_OFFS, 0);
	for (int i = 0; i < 8; i = i+1) {
		waitck(delay);
		IOWR_ALTERA_AVALON_PIO_DATA(SCK_BASE, 0);
		IOWR_ALTERA_AVALON_PIO_DATA(MOSI_BASE, (data >> (7-i)) & 1);
		waitck(delay);
		IOWR_ALTERA_AVALON_PIO_DATA(SCK_BASE, 1);
		tmp <<= 1;
		tmp |= IORD_ALTERA_AVALON_PIO_DATA(MISO_BASE);
	}
	waitck(delay);
//	IO_WR(SS_BASE, DATA_OFFS, 1);
	return tmp;
}
