/*
 * DMGLCD.c
 *
 * Created: 3/30/2018 2:27:03 PM
 *  Author: Bugs Bunny
 */ 

#include "DMGLCD.h"
#include  "unistd.h"

void DMGLCD_CS_HI() {
	IOWR_ALTERA_AVALON_PIO_DATA(SS_BASE, 1);
}
void DMGLCD_CS_LO() {
	IOWR_ALTERA_AVALON_PIO_DATA(SS_BASE, 0);
}
void DMGLCD_DC_DAT() {
	IOWR_ALTERA_AVALON_PIO_DATA(DC_BASE, 1);
}
void DMGLCD_DC_CMD() {
	IOWR_ALTERA_AVALON_PIO_DATA(DC_BASE, 0);
}
void DMGLCD_RST_HI() {
	IOWR_ALTERA_AVALON_PIO_DATA(RST_BASE, 1);
}
void DMGLCD_RST_LO() {
	IOWR_ALTERA_AVALON_PIO_DATA(RST_BASE, 0);
}

void DMGLCDInit(void)
{
	//IOWR_ALTERA_AVALON_PIO_DIRECTION(SS_BASE, 1);
	//IOWR_ALTERA_AVALON_PIO_DIRECTION(DC_BASE, 1);
	//IOWR_ALTERA_AVALON_PIO_DIRECTION(RST_BASE, 1);
	
	DMGLCD_RST_LO();

	int cnt = 0;
	while (cnt < 100) {
		cnt = cnt + 1;
	}

	DMGLCD_CS_HI();
	DMGLCD_DC_CMD();
	DMGLCD_RST_HI();
	
	#ifndef SPI_H_
	SPIInit();
	#endif
	
	LCDInit(); // Initialize the actual LCD
}

void LCDInit(void)
{
	DMGLCDWriteCommand(0xF0); // PASSWD1 - OK
	DMGLCDWriteData(0x5A);
	DMGLCDWriteData(0x5A);

	DMGLCDWriteCommand(0xF1); // PASSWD2 - OK
	DMGLCDWriteData(0x5A);
	DMGLCDWriteData(0x5A);

	DMGLCDWriteCommand(0xF2); // DISCTL (Display Control)
	DMGLCDWriteData(0x3B);    // 480 scan lines
	DMGLCDWriteData(0x33);
	DMGLCDWriteData(0x03);
	DMGLCDWriteData(0x0f);
	DMGLCDWriteData(0x0f);
	DMGLCDWriteData(0x08);
	DMGLCDWriteData(0x08);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x08);
	DMGLCDWriteData(0x08);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x33);
	DMGLCDWriteData(0x0f);
	DMGLCDWriteData(0x0f);
	DMGLCDWriteData(0x0f);
	DMGLCDWriteData(0x0f);

	DMGLCDWriteCommand(0xF4);
	DMGLCDWriteData(0x07);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x04);
	DMGLCDWriteData(0x70);
	DMGLCDWriteData(0x03);
	DMGLCDWriteData(0x04);
	DMGLCDWriteData(0x70);
	DMGLCDWriteData(0x03);

	DMGLCDWriteCommand(0xF5);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x46);//Set VCOMH
	DMGLCDWriteData(0x70);//Set VCOM Amplitude
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x02);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x46);
	DMGLCDWriteData(0x70);

	DMGLCDWriteCommand(0xF6);
	DMGLCDWriteData(0x03);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x08);
	DMGLCDWriteData(0x03);
	DMGLCDWriteData(0x03);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x03);
	DMGLCDWriteData(0x00);

	DMGLCDWriteCommand(0xF7);
	DMGLCDWriteData(0x48);
	DMGLCDWriteData(0x80);//RGB_MCU(DM)
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x02);
	DMGLCDWriteData(0x00);
	
	DMGLCDWriteCommand(0xF8);
	DMGLCDWriteData(0x11);
	DMGLCDWriteData(0x00);

	DMGLCDWriteCommand(0xF9); //Gamma Selection
	DMGLCDWriteData(0x14);

	DMGLCDWriteCommand(0xFA); //Positive Gamma Control
	DMGLCDWriteData(0x33);
	DMGLCDWriteData(0x07);
	DMGLCDWriteData(0x04);
	DMGLCDWriteData(0x1A);
	DMGLCDWriteData(0x18);
	DMGLCDWriteData(0x1C);
	DMGLCDWriteData(0x24);
	DMGLCDWriteData(0x1D);
	DMGLCDWriteData(0x26);
	DMGLCDWriteData(0x28);
	DMGLCDWriteData(0x2F);
	DMGLCDWriteData(0x2E);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);

	DMGLCDWriteCommand(0xFB); // Positive Gamma Control
	DMGLCDWriteData(0x33);
	DMGLCDWriteData(0x03);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x2E);
	DMGLCDWriteData(0x2F);
	DMGLCDWriteData(0x28);
	DMGLCDWriteData(0x26);
	DMGLCDWriteData(0x1D);
	DMGLCDWriteData(0x24);
	DMGLCDWriteData(0x1C);
	DMGLCDWriteData(0x18);
	DMGLCDWriteData(0x1A);
	DMGLCDWriteData(0x04);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);

	DMGLCDWriteCommand(0xF9); // Gamma Selection
	DMGLCDWriteData(0x12);

	DMGLCDWriteCommand(0xFA); // Positive Gamma Control
	DMGLCDWriteData(0x36);
	DMGLCDWriteData(0x07);
	DMGLCDWriteData(0x04);
	DMGLCDWriteData(0x1C);
	DMGLCDWriteData(0x1C);
	DMGLCDWriteData(0x23);
	DMGLCDWriteData(0x28);
	DMGLCDWriteData(0x1C);
	DMGLCDWriteData(0x25);
	DMGLCDWriteData(0x26);
	DMGLCDWriteData(0x2E);
	DMGLCDWriteData(0x2B);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);

	DMGLCDWriteCommand(0xFB); // Positive Gamma Control
	DMGLCDWriteData(0x33);
	DMGLCDWriteData(0x06);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x2B);
	DMGLCDWriteData(0x2E);
	DMGLCDWriteData(0x26);
	DMGLCDWriteData(0x25);
	DMGLCDWriteData(0x1C);
	DMGLCDWriteData(0x28);
	DMGLCDWriteData(0x23);
	DMGLCDWriteData(0x1C);
	DMGLCDWriteData(0x1C);
	DMGLCDWriteData(0x04);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);

	DMGLCDWriteCommand(0xF9); // Gamma Selection
	DMGLCDWriteData(0x11);

	DMGLCDWriteCommand(0xFA); // Positive Gamma Control
	DMGLCDWriteData(0x33);
	DMGLCDWriteData(0x07);
	DMGLCDWriteData(0x04);
	DMGLCDWriteData(0x30);
	DMGLCDWriteData(0x32);
	DMGLCDWriteData(0x34);
	DMGLCDWriteData(0x35);
	DMGLCDWriteData(0x11);
	DMGLCDWriteData(0x1D);
	DMGLCDWriteData(0x20);
	DMGLCDWriteData(0x28);
	DMGLCDWriteData(0x20);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);

	DMGLCDWriteCommand(0xFB); // Positive Gamma Control
	DMGLCDWriteData(0x33);
	DMGLCDWriteData(0x03);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x20);
	DMGLCDWriteData(0x28);
	DMGLCDWriteData(0x20);
	DMGLCDWriteData(0x1D);
	DMGLCDWriteData(0x11);
	DMGLCDWriteData(0x35);
	DMGLCDWriteData(0x34);
	DMGLCDWriteData(0x32);
	DMGLCDWriteData(0x30);
	DMGLCDWriteData(0x04);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);

	DMGLCDWriteCommand(0x44);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x01);
	
	DMGLCDWriteCommand(0x2A);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x01);
	DMGLCDWriteData(0xDF);

	DMGLCDWriteCommand(0x2B);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x01);
	DMGLCDWriteData(0x3F);
	
	DMGLCDWriteCommand(0x36); // Memory Data Access Control
	DMGLCDWriteData(0x00);

	DMGLCDWriteCommand(0x3A); //SET 65K Color
	DMGLCDWriteData(0x55);

	DMGLCDWriteCommand(0x11);
	usleep(120);
//	_delay_ms(120);

	DMGLCDWriteCommand(0x29); // Display on
	DMGLCDWriteCommand(0x2C); // Write GRAM

	usleep(10);
//	_delay_ms(10);
	DMGLCDWriteCommand(0x36); // Set_address_mode
	DMGLCDWriteData(0x60); // ?
}

void DMGLCDTestColor()
{
	DMGLCDWriteCommand(0x2A);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x01);
	DMGLCDWriteData(0xDF);
	DMGLCDWriteCommand(0x2B);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x01);
	DMGLCDWriteData(0x3F);
	DMGLCDWriteCommand(0x2C);
	int i = 0;
	int j = 0;
	int cnt = 0;
	for(i = 0; i < 480; i++) // Y
	{
		for(j = 0; j < 320; j++)
		{
			if (i < 120)
			{
				DMGLCDWriteData(RED>>8);
				DMGLCDWriteData((alt_u8)RED);
			}
			else if (i < 240)
			{
				DMGLCDWriteData(BLUE>>8);
				DMGLCDWriteData((alt_u8)BLUE);
			}
			else if (i < 360)
			{
				DMGLCDWriteData(GREEN>>8);
				DMGLCDWriteData((alt_u8)GREEN);
			}
			else
			{
				DMGLCDWriteData(YELLOW>>8);
				DMGLCDWriteData((alt_u8)YELLOW);
			}
		}
	}
}

void DMGLCDWriteCommand(alt_u8 cmd)
{
	DMGLCD_DC_CMD();
	DMGLCD_CS_LO();
	
	SPITransaction(cmd);
	
	DMGLCD_CS_HI();
}

void DMGLCDWriteData(alt_u8 data)
{
	DMGLCD_DC_DAT();
	DMGLCD_CS_LO();
	
	SPITransaction(data);
	
	DMGLCD_CS_HI();
}
