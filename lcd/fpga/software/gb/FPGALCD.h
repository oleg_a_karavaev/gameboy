/*
 * FPGALCD.h
 * 
 * Created: 3/20/2018 4:00:44 PM
 *  Author: Bugs Bunny
 */ 

#include "baseinclude.h"

#ifndef FPGALCD_H_
#define FPGALCD_H_

// FPGA register addresses
#define FPGALCD_CTRL				0x00
#define FPGALCD_BACK_RGB			0x04
#define FPGALCD_FRONT_RGB			0x08
#define FPGALCD_FRAME_SIZE			0x0c
#define FPGALCD_HOR_SYNCS_LENGTH	0x10
#define FPGALCD_VER_SYNCS_LENGTH	0x14

// FPGA bit masks
#define FPGALCD_RESET_BM		0x80000000
#define FPGALCD_FRAME_BUF_BM	0x40000000
#define FPGALCD_BICUBIC_BM		0x20000000
#define FPGALCD_EN				0x00000100
#define FPGALCD_WBSOURCE		0x00000200
#define FPGALCD_CLR				0x00000001
#define FPGALCD_LOAD			0x00000008

// SYNC LENGTH POS
#define FPGALCD_SYNCS_LENGTH_SIZE	10
#define FPGALCD_BPW_POS				0
#define FPGALCD_FPW_POS				10
#define FPGALCD_SW_POS				20

// FRAME SIZE POS
#define FPGALCD_FRAMESZ_SIZE	16
#define FPGALCD_HSIZE_POS	0
#define FPGALCD_VSIZE_POS	16

#include <stdbool.h>
#include "SPI.h"


void FPGALCD_CS_HI();
void FPGALCD_CS_LO();
void		FPGALCDInit(void);
alt_u32	FPGALCDGetBackgroundRGB(void);
void		FPGALCDSetBackgroundRGB(alt_u32 rgb);
alt_u32	FPGALCDGetForegroundRGB(void);
void		FPGALCDSetForegroundRGB(alt_u32 rgb);
void		FPGALCDReset(void);
void		DMGFrameBufferMode(bool on);
void		DMGFrameBicubicIntrpMode(bool on);
//void		FPGALCDTeeMode(bool on);

alt_u32 FPGALCDReadRegister(alt_u8 address);
void FPGALCDWriteRegister(alt_u8 address, alt_u32 data);

#endif /* FPGALCD_H_ */
