/*
 * "Hello World" example.
 *
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example
 * designs. It runs with or without the MicroC/OS-II RTOS and requires a STDOUT
 * device in your system's hardware.
 * The memory footprint of this hosted application is ~69 kbytes by default
 * using the standard reference design.
 *
 * For a reduced footprint version of this template, and an explanation of how
 * to reduce the memory footprint for a given application, see the
 * "small_hello_world" template.
 *
 */

#include <stdio.h>
//#include <avr/io.h>
//#include <avr/interrupt.h>
#include "baseinclude.h"
#include "SPI.h"
#include "DMGLCD.h"
#include "FPGALCD.h"

int main(void)
{
	printf("Hello from Nios II!\n");
	SPIInit();
	DMGLCDInit();
	FPGALCDInit();

//	DMGLCDTestColor();
/*	alt_u32 tmp = 75;
	tmp <<= 8;
	tmp |= 49;
	tmp <<= 8;
	tmp |= 119;
	FPGALCDSetBackgroundRGB(tmp);
	tmp = 138;
	tmp <<= 8;
	tmp |= 243;
	tmp <<= 8;
	tmp |= 75;
	FPGALCDSetForegroundRGB(tmp);*/

	DMGLCDWriteCommand(0xF7);
	DMGLCDWriteData(0x48);
	DMGLCDWriteData(0x81);//RGB_MCU(DM)
	//DMGLCDWriteData(0x82);//Set VSYNC Interface
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x02);
	DMGLCDWriteData(0x00);

/*	DMGLCDWriteCommand(0x2A);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x01);
	DMGLCDWriteData(0xDF);
	DMGLCDWriteCommand(0x2B);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x00);
	DMGLCDWriteData(0x01);
	DMGLCDWriteData(0x3F);
*/

//	IOWR(LCD_BRIDGE_0_BASE, FPGALCD_CTRL, FPGALCD_EN);// | FPGALCD_WBSOURCE);
	int data = 0;
//	data = IORD(LCD_BRIDGE_0_BASE, FPGALCD_CTRL);
//	IOWR(LCD_BRIDGE_0_BASE, FPGALCD_CTRL, 1);
	IOWR(LCD_BRIDGE_0_BASE, FPGALCD_CTRL, FPGALCD_BICUBIC_BM | FPGALCD_EN | FPGALCD_WBSOURCE);
//	IOWR(LCD_BRIDGE_0_BASE, FPGALCD_CTRL, FPGALCD_EN | FPGALCD_WBSOURCE);
	data = 0;
	data = IORD(LCD_BRIDGE_0_BASE, FPGALCD_CTRL);
	//FPGALCDWriteRegister(FPGALCD_CTRL, FPGALCD_EN);
//	FPGALCDWriteRegister(FPGALCD_CTRL, FPGALCD_EN | FPGALCD_LOAD);
    while (1)
    {}
}

