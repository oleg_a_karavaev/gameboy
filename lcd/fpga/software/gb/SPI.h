/*
 * SPI.h
 *
 *  Created on: 09 ����. 2018 �.
 *      Author: Dell
 */

#ifndef SPI_H_
#define SPI_H_

#include "baseinclude.h"

#define SPI_LEN 4



void SPIInit(void);
alt_u8 SPITransaction(alt_u8 data);

#endif /* SPI_H_ */
