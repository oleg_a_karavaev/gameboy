/*
 * FPGALCD.c
 *
 * Created: 3/20/2018 4:05:49 PM
 *  Author: Bugs Bunny
 */ 

#include "FPGALCD.h"


void FPGALCDInit(void)
{
	IOWR(LCD_BRIDGE_0_BASE, FPGALCD_CTRL, FPGALCD_CLR);
	//FPGALCDReset();
	int data = 0;
//	alt_u32 wdata = 46072287;
//	IOWR(LCD_BRIDGE_0_BASE, FPGALCD_FRAME_SIZE, wdata);
//	IOWR(LCD_BRIDGE_0_BASE, FPGALCD_HOR_SYNCS_LENGTH, 15818855);
//	IOWR(LCD_BRIDGE_0_BASE, FPGALCD_VER_SYNCS_LENGTH, 21153990);

	IOWR(LCD_BRIDGE_0_BASE, FPGALCD_FRAME_SIZE, (510 << 16) | 350);
	data = IORD(LCD_BRIDGE_0_BASE, FPGALCD_FRAME_SIZE);

	IOWR(LCD_BRIDGE_0_BASE, FPGALCD_HOR_SYNCS_LENGTH, (10 << 20) | (15 << 10) | 15);
	IOWR(LCD_BRIDGE_0_BASE, FPGALCD_VER_SYNCS_LENGTH, (10 << 20) | (15 << 10) | 15);
	IOWR(LCD_BRIDGE_0_BASE, FPGALCD_FRONT_RGB, 0x00ffff00);
	IOWR(LCD_BRIDGE_0_BASE, FPGALCD_BACK_RGB, 0x000000ff);
	//data = IORD(LCD_BRIDGE_0_BASE | 12, 0);
	data = IORD(LCD_BRIDGE_0_BASE, FPGALCD_HOR_SYNCS_LENGTH);
	data = IORD(LCD_BRIDGE_0_BASE, FPGALCD_VER_SYNCS_LENGTH);
}

alt_u32 FPGALCDGetBackgroundRGB(void)
{
	return FPGALCDReadRegister(FPGALCD_BACK_RGB);
}

void FPGALCDSetBackgroundRGB(alt_u32 rgb)
{
	FPGALCDWriteRegister(FPGALCD_BACK_RGB, rgb);
}

alt_u32 FPGALCDGetForegroundRGB(void)
{
	return FPGALCDReadRegister(FPGALCD_FRONT_RGB);
}

void FPGALCDSetForegroundRGB(alt_u32 rgb)
{
	FPGALCDWriteRegister(FPGALCD_FRONT_RGB, rgb);
}

void FPGALCDReset(void)
{
	alt_u32 tempVal = FPGALCDReadRegister(FPGALCD_CTRL);
	FPGALCDWriteRegister(FPGALCD_CTRL, tempVal | FPGALCD_RESET_BM);
}

void DMGFrameBufferMode(bool on)
{
	alt_u32 tempVal = FPGALCDReadRegister(FPGALCD_CTRL);
	
	if (on)
		tempVal |= FPGALCD_FRAME_BUF_BM;
	else
		tempVal &= ~FPGALCD_FRAME_BUF_BM;
	
	FPGALCDWriteRegister(FPGALCD_CTRL, tempVal);
}

void DMGFrameBicubicIntrpMode(bool on)
{
	alt_u32 tempVal = FPGALCDReadRegister(FPGALCD_CTRL);
	
	if (on)
		tempVal |= FPGALCD_BICUBIC_BM;
	else
		tempVal &= ~FPGALCD_BICUBIC_BM;
	
	FPGALCDWriteRegister(FPGALCD_CTRL, tempVal);
}

/*
void FPGALCDTeeMode(bool on)
{
	alt_u32 tempVal = FPGALCDReadRegister(FPGALCD_CTRL);
	
	if (on)
		tempVal |= 0x10000000;
	else
		tempVal &= ~0x10000000;
	
	FPGALCDWriteRegister(FPGALCD_CTRL, tempVal);
}
*/

alt_u32 FPGALCDReadRegister(alt_u8 address)
{
	alt_u32 returnVal = 0;
	
	returnVal = IORD(LCD_BRIDGE_0_BASE, address);
	
	return returnVal;
}

void FPGALCDWriteRegister(alt_u8 address, alt_u32 data)
{
	IOWR(LCD_BRIDGE_0_BASE, address, data);
}
