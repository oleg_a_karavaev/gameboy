module bicubic_alg_core #(parameter logic simplify = 1'b1) (
	  input clk_i
	, input nrst_i
	// , input [3:0] [11:0] x_rel_i
	// , input [3:0] [11:0] y_rel_i
	, input x_rel_i
	, input y_rel_i
	, input [3:0] [3:0] [2:0] [7:0] base_vector_i
	, input en_i
	, output [2:0] [7:0] pix_o
	, output valid_o
);
	logic signed [3:0] [3:0] [2:0] [15:0] p_tc, p;
	logic [2:0] carry;
	logic signed [2:0] [15:0] 
		  a22_r, a23_r, a32_r, a33_r, a12_r, a13_r, a21_r, a31_r, a00_r, a01_r, a02_r, a03_r, a10_r, a11_r, a20_r, a30_r
		, a12_ff0, a13_ff0, a21_ff0, a31_ff0, a00_ff0, a01_ff0, a02_ff0, a03_ff0, a10_ff0, a11_ff0, a20_ff0, a30_ff0
		, a00_ff1, a01_ff1, a02_ff1, a03_ff1, a10_ff1, a11_ff1, a20_ff1, a30_ff1
		, p_r, p0_r, p1_r, p2_r, p3_r, px0_r, px1_r, px2_r, px3_r
		, a00_r_ff0, a10_r_ff0, a20_r_ff0, a30_r_ff0
		, a12_part1_r, a12_part2_r, a13_part1_r, a13_part2_r, a21_part1_r, a21_part2_r, a22_part1_r, a31_part1_r
		, a22_part2_r, a23_part1_r, a23_part2_r, a32_part1_r, a32_part2_r, a33_part1_r, a33_part2_r, a31_part2_r
		, a22_part1_part1_r, a22_part1_part2_r, a22_part2_part1_r, a22_part2_part2_r, a23_part1_part1_r, a23_part1_part2_r, a23_part2_part1_r, a33_part2_part1_r
		, a23_part2_part2_r, a32_part1_part1_r, a32_part1_part2_r, a32_part2_part1_r, a32_part2_part2_r, a33_part1_part1_r, a33_part1_part2_r, a33_part2_part2_r;
	logic signed[2:0] [15:0]
		  a01_mul_y_r, a02_mul_y_r, a03_mul_y_r, a11_mul_y_r, a12_mul_y_r, a13_mul_y_r, a21_mul_y_r, a22_mul_y_r
		, a23_mul_y_r, a31_mul_y_r, a32_mul_y_r, a33_mul_y_r;
	logic signed [2:0] [15:0]
		  a01_mul_y_n, a02_mul_y_n, a03_mul_y_n, a11_mul_y_n, a12_mul_y_n, a13_mul_y_n, a21_mul_y_n, a22_mul_y_n
		, a23_mul_y_n, a31_mul_y_n, a32_mul_y_n, a33_mul_y_n;		
	// logic signed [3:0] [15:0] y_rel_ff2, y_rel_ff1, y_rel_ff0, x_rel_ff4, x_rel_ff3, x_rel_ff2, x_rel_ff1, x_rel_ff0;
	logic y_rel_ff2, y_rel_ff1, y_rel_ff0, x_rel_ff4, x_rel_ff3, x_rel_ff2, x_rel_ff1, x_rel_ff0;
	logic signed [2:0] [15:0] 
		  a22_n, a23_n, a32_n, a33_n, a12_n, a13_n, a21_n, a31_n, a00_n, a01_n, a02_n, a03_n, a10_n, a11_n, a20_n, a30_n
		, p_n, p0_n, px0_n, px1_n, px2_n, px3_n
		, a12_part1_n, a12_part2_n, a13_part1_n, a13_part2_n, a21_part1_n, a21_part2_n, a22_part1_n, a22_part2_n
		, a23_part1_n, a23_part2_n, a32_part1_n, a32_part2_n, a33_part1_n, a33_part2_n, a31_part1_n, a31_part2_n
		, a22_part1_part1_n, a22_part1_part2_n, a22_part2_part1_n, a22_part2_part2_n, a23_part1_part1_n, a23_part1_part2_n, a23_part2_part1_n, a23_part2_part2_n
		, a32_part1_part1_n, a32_part1_part2_n, a32_part2_part1_n, a32_part2_part2_n, a33_part1_part1_n, a33_part1_part2_n, a33_part2_part1_n, a33_part2_part2_n;
	logic signed [2:0] [15:0] p1_n, p2_n, p3_n;
	logic [6:0] valid_r;

	genvar i, j, k;
	
	generate
		for (i=0; i<4; i=i+1) begin : inp_matrix_y
			for (j=0; j<4; j=j+1) begin : inp_matrix_x
				for (k=0; k<3; k=k+1) begin : inp_matrix_color
					always @(negedge clk_i or negedge nrst_i)
						if (~nrst_i) begin
							p_tc[j][i][k] <= 0;
							p[j][i][k] <= 0;
						end
						else begin
							p_tc[j][i][k] <= ~{8'b0, base_vector_i[i][j][k]} + 1;
							p[j][i][k] <= {8'b0, base_vector_i[i][j][k]};
						end
				end
			end
		end
	endgenerate
	
	generate
		for (i=0; i<3; i=i+1) begin : comp_per_color
			
			// step 1
		
			assign a00_n[i] =				  p[1][1][i];
			assign a01_n[i] =				  (p[1][2][i] >>> 1) - (p[1][0][i] >>> 1)
			;
			assign a02_n[i] =				  p[1][0][i]
											- (p[1][1][i] << 1)
											- (p[1][1][i] >>> 1)
											+ (p[1][2][i] << 1)
											- (p[1][3][i] >>> 1)
			;
			assign a03_n[i] =				- (p[1][0][i] >>> 1)
											+ p[1][1][i]
											+ (p[1][1][i] >>> 1)
											- p[1][2][i]
											- (p[1][2][i] >>> 1)
											+ (p[1][3][i] >>> 1)
			;
			
			assign a10_n[i] =				- (p[0][1][i] >>> 1)
											+ (p[2][1][i] >>> 1)
			;
			assign a11_n[i] =				  (p[0][0][i] >>> 2)
											- (p[0][2][i] >>> 2)
											- (p[2][0][i] >>> 2)
											+ (p[2][2][i] >>> 2)
			;
			assign a12_part1_n[i] =			- (p[0][0][i] >>> 1)
											+ p[0][1][i]
											+ (p[0][1][i] >>> 2)
											- p[0][2][i]
											+ (p[0][3][i] >>> 2)
			;
			assign a12_part2_n[i] =			  (p[2][0][i] >>> 1)
											- p[2][1][i]
											- (p[2][1][i] >>> 2)
											+ p[2][2][i]
											- (p[2][3][i] >>> 2)
			;
			assign a13_part1_n[i] =			  (p[0][0][i] >>> 2)
											- p[0][1][i]
											+ (p[0][1][i] >>> 2)
											+ p[0][2][i]
											- (p[0][2][i] >>> 2)
											- (p[0][3][i] >>> 2)
			;
			assign a13_part2_n[i] =			- (p[2][0][i] >>> 2)
											+ p[2][1][i]
											- (p[2][1][i] >>> 2)
											- p[2][2][i]
											+ (p[2][2][i] >>> 2)
											+ (p[2][3][i] >>> 2)
			;
		
			assign a20_n[i] =				  p[0][1][i]
											- (p[1][1][i] << 1)
											- (p[1][1][i] >>> 1)
											+ (p[2][1][i] << 1)
											- (p[3][1][i] >>> 1)
			;
			assign a21_part1_n[i] =			- (p[0][0][i]>>> 1)
											+ (p[0][2][i]>>> 1)
											+ p[1][0][i]
											+ (p[1][0][i]>>> 2)
											- p[1][2][i]
											- (p[1][2][i]>>> 2)
			;
			assign a21_part2_n[i] =			- p[2][0][i]
											+ p[2][2][i]
											+ (p[3][0][i]>>> 2)
											- (p[3][2][i]>>> 2)
			;
			assign a22_part1_part1_n[i] =	  p[0][0][i]
											- (p[0][1][i]<< 1)
											- (p[0][1][i]>>> 1)
											+ (p[0][2][i]<< 1)
											- (p[0][3][i]>>> 1)
			;
			assign a22_part1_part2_n[i] =	- (p[1][0][i]<< 1)
											- (p[1][0][i]>>> 1)
											+ (p[1][1][i]<< 2)
											+ (p[1][1][i]<< 1)
											+ (p[1][1][i]>>> 2)
											- (p[1][2][i]<< 2)
											- p[1][2][i]
											+ p[1][3][i]
											+ (p[1][3][i]>>> 2)
			;
			assign a22_part2_part1_n[i] =	  (p[2][0][i]<< 1)
											- (p[2][1][i]<< 2)
											- p[2][1][i]
											+ (p[2][2][i]<< 2)
											- p[2][3][i]
			;
			assign a22_part2_part2_n[i] =	- (p[3][0][i]>>> 1)
											+ p[3][1][i]
											+ (p[3][1][i]>>> 2)
											- p[3][2][i]
											+ (p[3][3][i]>>> 2)
			;
			assign a23_part1_part1_n[i] =	- (p[0][0][i]>>> 1)
											+ p[0][1][i]
											+ (p[0][1][i]>>> 1)
											- p[0][2][i]
											- (p[0][2][i]>>> 1)
											+ (p[0][3][i]>>> 1)
			;
			assign a23_part1_part2_n[i] =	  p[1][0][i]
											+ (p[1][0][i]>>> 2)
											- (p[1][1][i]<< 2)
											+ (p[1][1][i]>>> 2)
											+ (p[1][2][i]<< 2)
											- (p[1][2][i]>>> 2)
											- p[1][3][i]
											- (p[1][3][i]>>> 2)
			;
			assign a23_part2_part1_n[i] =	- p[2][0][i]
											+ (p[2][1][i]<< 1)
											+ p[2][1][i]
											- (p[2][2][i]<< 1)
											- p[2][2][i]
											+ p[2][3][i]
			;
			assign a23_part2_part2_n[i] =	  (p[3][0][i]>>> 2)
											- p[3][1][i]
											+ (p[3][1][i]>>> 2)
											+ p[3][2][i]
											- (p[3][2][i]>>> 2)
											- (p[3][3][i]>>> 2)
			;
			assign a30_n[i] =				- (p[0][1][i]>>> 1)
											+ p[1][1][i]
											+ (p[1][1][i]>>> 1)
											- p[2][1][i]
											- (p[2][1][i]>>> 1)
											+ (p[3][1][i]>>> 1)
			;
			
			assign a31_part1_n[i] =			  (p[0][0][i]>>> 2)
											- (p[0][2][i]>>> 2)
											- (p[1][0][i]>>> 1)
											- (p[1][0][i]>>> 2)
											+ (p[1][2][i]>>> 1)
											+ (p[1][2][i]>>> 2)
			;
			assign a31_part2_n[i] =			  (p[2][0][i]>>> 2)
											+ (p[2][0][i]>>> 1)
											- (p[2][2][i]>>> 2)
											- (p[2][2][i]>>> 1)
											- (p[3][0][i]>>> 2)
											+ (p[3][2][i]>>> 2)
			;
			
			assign a32_part1_part1_n[i] =	- (p[0][0][i]>>> 1)
											+ p[0][1][i]
											+ (p[0][1][i]>>> 2)
											- p[0][2][i]
											+ (p[0][3][i]>>> 2)
			;
			assign a32_part1_part2_n[i] =	  p[1][0][i]
											+ (p[1][0][i]>>> 1)
											- (p[1][1][i]<< 2)
											+ (p[1][1][i]>>> 2)
											+ p[1][2][i]
											+ (p[1][2][i]<< 1)
											- (p[1][3][i]>>> 1)
											- (p[1][3][i]>>> 2)
			;
			assign a32_part2_part1_n[i] =	- p[2][0][i]
											- (p[2][0][i]>>> 1)
											+ (p[2][1][i]<< 2)
											- (p[2][1][i]>>> 2)
											- (p[2][2][i]<< 1)
											- p[2][2][i]
											+ (p[2][3][i]>>> 1)
											+ (p[2][3][i]>>> 2)
			;
			assign a32_part2_part2_n[i] =	  (p[3][0][i]>>> 1)
											- p[3][1][i]
											- (p[3][1][i]>>> 2)
											+ p[3][2][i]
											- (p[3][3][i]>>> 2)
			;
			assign a33_part1_part1_n[i] =	  (p[0][0][i]>>> 2)
											- (p[0][1][i]>>> 1)
											- (p[0][1][i]>>> 2)
											+ (p[0][2][i]>>> 1)
											+ (p[0][2][i]>>> 2)
											- (p[0][3][i]>>> 2)
			;
			assign a33_part1_part2_n[i] =	- (p[1][0][i]>>> 1)
											- (p[1][0][i]>>> 2)
											+ (p[1][1][i]<< 1)
											+ (p[1][1][i]>>> 2)
											- (p[1][2][i]<< 1)
											- (p[1][2][i]>>> 2)
											+ (p[1][3][i]>>> 1)
											+ (p[1][3][i]>>> 2)
			;
			assign a33_part2_part1_n[i] =	  (p[2][0][i]>>> 1)
											+ (p[2][0][i]>>> 2)
											- (p[2][1][i]<< 1)
											- (p[2][1][i]>>> 2)
											+ (p[2][2][i]<< 1)
											+ (p[2][2][i]>>> 2)
											- (p[2][3][i]>>> 1)
											- (p[2][3][i]>>> 2)
			;
			assign a33_part2_part2_n[i] =	- (p[3][0][i]>>> 2)
											+ (p[3][1][i]>>> 1)
											+ (p[3][1][i]>>> 2)
											- (p[3][2][i]>>> 1)
											- (p[3][2][i]>>> 2)
											+ (p[3][3][i]>>> 2)
			;
			
			// step 2
			
			assign a12_n[i] = a12_part1_r[i] + a12_part2_r[i];
			assign a13_n[i] = a13_part1_r[i] + a13_part2_r[i];
			
			assign a21_n[i] = a21_part1_r[i] + a21_part2_r[i];
			assign a22_part1_n[i] = a22_part1_part1_r[i] + a22_part1_part2_r[i];
			assign a22_part2_n[i] = a22_part2_part1_r[i] + a22_part2_part2_r[i];
			assign a23_part1_n[i] = a23_part1_part1_r[i] + a23_part1_part2_r[i];
			assign a23_part2_n[i] = a23_part2_part1_r[i] + a23_part2_part2_r[i];
			
			assign a31_n[i] = a31_part1_r[i] + a31_part2_r[i];
			assign a32_part1_n[i] = a32_part1_part1_r[i] + a32_part1_part2_r[i];
			assign a32_part2_n[i] = a32_part2_part1_r[i] + a32_part2_part2_r[i];
			assign a33_part1_n[i] = a33_part1_part1_r[i] + a33_part1_part2_r[i];
			assign a33_part2_n[i] = a33_part2_part1_r[i] + a33_part2_part2_r[i];
			
			// step 3
			
			assign a22_n[i] = a22_part1_r[i] + a22_part2_r[i];
			assign a23_n[i] = a23_part1_r[i] + a23_part2_r[i];
			
			assign a32_n[i] = a32_part1_r[i] + a32_part2_r[i];
			assign a33_n[i] = a33_part1_r[i] + a33_part2_r[i];
			
			
			// final computations
// if (simplify) begin : simple_gen
			assign a01_mul_y_n[i] = y_rel_ff2 ? 0 : ($signed(a01_r[i]) >>> 1);
			assign a02_mul_y_n[i] = y_rel_ff2 ? 0 : ($signed(a02_r[i]) >>> 2);
			assign a03_mul_y_n[i] = y_rel_ff2 ? 0 : ($signed(a03_r[i]) >>> 3);
			
			assign a11_mul_y_n[i] = y_rel_ff2 ? 0 : ($signed(a11_r[i]) >>> 1);
			assign a12_mul_y_n[i] = y_rel_ff2 ? 0 : ($signed(a12_r[i]) >>> 2);
			assign a13_mul_y_n[i] = y_rel_ff2 ? 0 : ($signed(a13_r[i]) >>> 3);
			
			assign a21_mul_y_n[i] = y_rel_ff2 ? 0 : ($signed(a21_r[i]) >>> 1);
			assign a22_mul_y_n[i] = y_rel_ff2 ? 0 : ($signed(a22_r[i]) >>> 2);
			assign a23_mul_y_n[i] = y_rel_ff2 ? 0 : ($signed(a23_r[i]) >>> 3);
			
			assign a31_mul_y_n[i] = y_rel_ff2 ? 0 : ($signed(a31_r[i]) >>> 1);
			assign a32_mul_y_n[i] = y_rel_ff2 ? 0 : ($signed(a32_r[i]) >>> 2);
			assign a33_mul_y_n[i] = y_rel_ff2 ? 0 : ($signed(a33_r[i]) >>> 3);
			
			assign px0_n[i] = a00_r_ff0[i] + a01_mul_y_r[i] + a02_mul_y_r[i] + a03_mul_y_r[i];
			assign px1_n[i] = a10_r_ff0[i] + a11_mul_y_r[i] + a12_mul_y_r[i] + a13_mul_y_r[i];
			assign px2_n[i] = a20_r_ff0[i] + a21_mul_y_r[i] + a22_mul_y_r[i] + a23_mul_y_r[i];
			assign px3_n[i] = a30_r_ff0[i] + a31_mul_y_r[i] + a32_mul_y_r[i] + a33_mul_y_r[i];
			
			assign p0_n[i] = px0_r[i];
			assign p1_n[i] = x_rel_ff4 ? 0 : ($signed(px1_r[i]) >>> 1);
			assign p2_n[i] = x_rel_ff4 ? 0 : ($signed(px2_r[i]) >>> 2);
			assign p3_n[i] = x_rel_ff4 ? 0 : ($signed(px3_r[i]) >>> 3);
// end // if (simplify)
// else begin : full_gen
// 			assign a01_mul_y_n[i] = {a01_r[i], 4'd0} * y_rel_ff2[1];
// 			assign a02_mul_y_n[i] = {a02_r[i], 4'd0} * y_rel_ff2[2];
// 			assign a03_mul_y_n[i] = {a03_r[i], 4'd0} * y_rel_ff2[3];
			
// 			assign a11_mul_y_n[i] = {a11_r[i], 4'd0} * y_rel_ff2[1];
// 			assign a12_mul_y_n[i] = {a12_r[i], 4'd0} * y_rel_ff2[2];
// 			assign a13_mul_y_n[i] = {a13_r[i], 4'd0} * y_rel_ff2[3];
			
// 			assign a21_mul_y_n[i] = {a21_r[i], 4'd0} * y_rel_ff2[1];
// 			assign a22_mul_y_n[i] = {a22_r[i], 4'd0} * y_rel_ff2[2];
// 			assign a23_mul_y_n[i] = {a23_r[i], 4'd0} * y_rel_ff2[3];
			
// 			assign a31_mul_y_n[i] = {a31_r[i], 4'd0} * y_rel_ff2[1];
// 			assign a32_mul_y_n[i] = {a32_r[i], 4'd0} * y_rel_ff2[2];
// 			assign a33_mul_y_n[i] = {a33_r[i], 4'd0} * y_rel_ff2[3];
			
// 			assign px0_n[i] = a00_r_ff0[i] + a01_mul_y_r[i][31:20] + a02_mul_y_r[i][31:20] + a03_mul_y_r[i][31:20];
// 			assign px1_n[i] = a10_r_ff0[i] + a11_mul_y_r[i][31:20] + a12_mul_y_r[i][31:20] + a13_mul_y_r[i][31:20];
// 			assign px2_n[i] = a20_r_ff0[i] + a21_mul_y_r[i][31:20] + a22_mul_y_r[i][31:20] + a23_mul_y_r[i][31:20];
// 			assign px3_n[i] = a30_r_ff0[i] + a31_mul_y_r[i][31:20] + a32_mul_y_r[i][31:20] + a33_mul_y_r[i][31:20];
			
// 			assign p0_n[i] = px0_r[i];
// 			assign p1_n[i] = {px1_r[i], 4'd0} * x_rel_ff4[1];
// 			assign p2_n[i] = {px2_r[i], 4'd0} * x_rel_ff4[2];
// 			assign p3_n[i] = {px3_r[i], 4'd0} * x_rel_ff4[3];
// end // else
			assign p_n[i] = p0_r[i] + p1_r[i] + p2_r[i] + p3_r[i];
		end
	endgenerate
	
	integer ix, jx;

// generate
// 	if (simplify) begin : simple_res_gen
		always @(negedge clk_i)
		begin
			for (ix=0; ix<3; ix=ix+1) begin
				p1_r[ix] <= p1_n[ix];
				p2_r[ix] <= p2_n[ix];
				p3_r[ix] <= p3_n[ix];
			end
		end
// 	end // simple_res_gen
// 	else begin : full_res_gen
// 		always @(negedge clk_i)
// 		begin
// 			for (ix=0; ix<3; ix=ix+1) begin
// 				p1_r[ix] <= p1_n[ix][19:8];
// 				p2_r[ix] <= p2_n[ix][19:8];
// 				p3_r[ix] <= p3_n[ix][19:8];
// 			end
// 		end
// 	end // else
// endgenerate
logic signed [2:0] [15:0] p_carry;

always @(*) for (int i = 0; i < 3; i++) begin
	p_carry[i][15:1] = 14'd0;
	p_carry[i][0] = p_n[i][8];
end

	always @(negedge clk_i or negedge nrst_i)
		if (~nrst_i) begin
		end
		else begin
			a22_r <= a22_n;
			a23_r <= a23_n;
			a32_r <= a32_n;
			a33_r <= a33_n;
			a12_r <= a12_ff0;
			a13_r <= a13_ff0;
			a21_r <= a21_ff0;
			a31_r <= a31_ff0;
			a00_r <= a00_ff1;
			a01_r <= a01_ff1;
			a02_r <= a02_ff1;
			a03_r <= a03_ff1;
			a10_r <= a10_ff1;
			a11_r <= a11_ff1;
			a20_r <= a20_ff1;
			a30_r <= a30_ff1;
			a12_ff0 <= a12_n;
			a13_ff0 <= a13_n;
			a21_ff0 <= a21_n;
			a31_ff0 <= a31_n;
			a00_ff0 <= a00_n;
			a01_ff0 <= a01_n;
			a02_ff0 <= a02_n;
			a03_ff0 <= a03_n;
			a10_ff0 <= a10_n;
			a11_ff0 <= a11_n;
			a20_ff0 <= a20_n;
			a30_ff0 <= a30_n;
			a00_ff1 <= a00_ff0;
			a01_ff1 <= a01_ff0;
			a02_ff1 <= a02_ff0;
			a03_ff1 <= a03_ff0;
			a10_ff1 <= a10_ff0;
			a11_ff1 <= a11_ff0;
			a20_ff1 <= a20_ff0;
			a30_ff1 <= a30_ff0;
			for (int i = 0; i < 3; i++) /*p_r[i] <= p_n[i] - p_carry[i];*/
				p_r[i] <= p_n[i][15] ? 16'd0 : (p_n[i] > 16'd255 ? 16'd255 : p_n[i]);
			p0_r <= p0_n;
			px0_r <= px0_n;
			px1_r <= px1_n;
			px2_r <= px2_n;
			px3_r <= px3_n;
			a00_r_ff0 <= a00_r;
			a10_r_ff0 <= a10_r;
			a20_r_ff0 <= a20_r;
			a30_r_ff0 <= a30_r;
			a01_mul_y_r <= a01_mul_y_n;
			a02_mul_y_r <= a02_mul_y_n;
			a03_mul_y_r <= a03_mul_y_n;
			a11_mul_y_r <= a11_mul_y_n;
			a12_mul_y_r <= a12_mul_y_n;
			a13_mul_y_r <= a13_mul_y_n;
			a21_mul_y_r <= a21_mul_y_n;
			a22_mul_y_r <= a22_mul_y_n;
			a23_mul_y_r <= a23_mul_y_n;
			a31_mul_y_r <= a31_mul_y_n;
			a32_mul_y_r <= a32_mul_y_n;
			a33_mul_y_r <= a33_mul_y_n;
			y_rel_ff2 <= y_rel_ff1;
			y_rel_ff1 <= y_rel_ff0;
			y_rel_ff0 <= y_rel_i;
			// for (ix=0; ix<4; ix=ix+1) begin
			// 	y_rel_ff0[ix] <= {4'd0, y_rel_i[ix]};
			// end
			x_rel_ff4 <= x_rel_ff3;
			x_rel_ff3 <= x_rel_ff2;
			x_rel_ff2 <= x_rel_ff1;
			x_rel_ff1 <= x_rel_ff0;
			x_rel_ff0 <= x_rel_i;
			// for (ix=0; ix<4; ix=ix+1) begin
			// 	x_rel_ff0[ix] <= {4'd0, x_rel_i[ix]};
			// end
			valid_r <= {valid_r[5:0], en_i};
			
			a12_part1_r <= a12_part1_n;
			a12_part2_r <= a12_part2_n;
			a13_part1_r <= a13_part1_n;
			a13_part2_r <= a13_part2_n;
			a21_part1_r <= a21_part1_n;
			a21_part2_r <= a21_part2_n;
			a22_part1_r <= a22_part1_n;
			a22_part2_r <= a22_part2_n;
			a23_part1_r <= a23_part1_n;
			a23_part2_r <= a23_part2_n;
			a32_part1_r <= a32_part1_n;
			a32_part2_r <= a32_part2_n;
			a33_part1_r <= a33_part1_n;
			a33_part2_r <= a33_part2_n;
			a31_part1_r <= a31_part1_n;
			a31_part2_r <= a31_part2_n;
			a22_part1_part1_r <= a22_part1_part1_n;
			a22_part1_part2_r <= a22_part1_part2_n;
			a22_part2_part1_r <= a22_part2_part1_n;
			a22_part2_part2_r <= a22_part2_part2_n;
			a23_part1_part1_r <= a23_part1_part1_n;
			a23_part1_part2_r <= a23_part1_part2_n;
			a23_part2_part1_r <= a23_part2_part1_n;
			a23_part2_part2_r <= a23_part2_part2_n;
			a32_part1_part1_r <= a32_part1_part1_n;
			a32_part1_part2_r <= a32_part1_part2_n;
			a32_part2_part1_r <= a32_part2_part1_n;
			a32_part2_part2_r <= a32_part2_part2_n;
			a33_part1_part1_r <= a33_part1_part1_n;
			a33_part1_part2_r <= a33_part1_part2_n;
			a33_part2_part1_r <= a33_part2_part1_n;
			a33_part2_part2_r <= a33_part2_part2_n;
			
		end
		
	assign pix_o = {p_r[2][7:0], p_r[1][7:0], p_r[0][7:0]};
	assign valid_o = valid_r[6];

endmodule