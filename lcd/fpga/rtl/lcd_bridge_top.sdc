create_clock -name "wb_clk" -period "4MHz" [get_ports wb_clk_i] -add
create_clock -name "clk" -period "50MHz" [get_ports clk_i] -add
create_clock -name "clk20" -period "20MHz" [get_ports clk20_i] -add

set_clock_groups -asynchronous -group {wb_clk} -group {clk} -group {clk20}