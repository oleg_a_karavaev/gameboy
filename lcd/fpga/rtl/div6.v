module div6(
	  input clk_i
	, input nrst_i
	, input [7:0] op_i
	, output [7:0] q_o
);
	
	reg [7:0] q;
	
	always @(posedge clk_i or negedge nrst_i)
		if (~nrst_i)
			q <= 8'd0;
		else
			q <= op_i / 8'h06;
		
	assign q_o = q;

endmodule