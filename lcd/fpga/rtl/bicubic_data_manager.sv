module bicubic_data_manager (
	  input nrst_i
	, input clk_i
	, input en_i
	, input [31:0] pixels_i
	, input pixels_valid_i
	, input [7:0] bg_r_i
	, input [7:0] bg_g_i
	, input [7:0] bg_b_i
	, input [7:0] c_r_i
	, input [7:0] c_g_i
	, input [7:0] c_b_i
	, input frame_start_i
	, input line_start_i
	, output switch_to_next_line_en_o
	, output field_request_o
	, output [2:0] [7:0] pixels_color_o
	, output pixels_color_valid_o
	, output repeat_request_enable_o
	, output to_next_line_enable_o
);

logic [0:7][0:15][1:0] pixbuff;
logic wpixbuffptr = 1'b0;
logic rpixbuffptr = 1'b0;
logic [3:0][3:0][2:0][7:0] base_vector;
logic [0:15][1:0] wb_pixels;
logic [7:0] en = 8'd0;

always @(posedge clk_i) en <= {en[6:0], pixels_valid_i};


logic [9:0] tgt_h_cnt = 10'd0, tgt_v_cnt = 10'd0;
logic [8:0] src_h_cnt, src_v_cnt;

assign src_v_cnt = tgt_v_cnt[9:1];
assign src_h_cnt = tgt_h_cnt[9:1];

logic next_h, next_v;
assign next_h = tgt_h_cnt[0];
assign next_v = tgt_v_cnt[0];
assign to_next_line_enable_o = next_v;
logic field_request = 1'b0;
assign field_request_o = field_request;

logic frame_processing = 1'b0;

logic [1:0] pixcnt = 2'd0;
logic line_processing = 1'b0;
logic repeat_request_enable = 1'b0;
assign repeat_request_enable_o = repeat_request_enable;

assign switch_to_next_line_en_o = tgt_v_cnt > 9'd2 && src_v_cnt < 9'd140;

logic first_reload = 1'b0;
logic line_processing_ff = 1'b0;

always @(posedge clk_i) begin
	field_request <= 1'b0;
	line_processing_ff <= line_processing;
	if (frame_start_i) begin
		frame_processing <= 1'b1;
		wpixbuffptr <= 1'b0;
		repeat_request_enable <= 1'b0;
	end // if (frame_start_i)
	if (frame_processing && line_start_i) begin
		field_request <= 1'b1;
		wpixbuffptr <= 1'b0;
		repeat_request_enable <= 1'b1;
		first_reload <= 1'b1;
	end
	if (frame_processing) begin
		if (en[0] & ~pixels_valid_i) line_processing <= 1'b1;
	end
	if (frame_processing && line_processing) begin
		tgt_h_cnt <= tgt_h_cnt + 1;
		if (tgt_h_cnt > 10'd2 && tgt_h_cnt < 10'd314 && tgt_h_cnt[0]) begin
			pixbuff[0] <= {pixbuff[0][1:15], pixbuff[4][0]};
			pixbuff[4] <= {pixbuff[4][1:15], 2'b00};
			pixbuff[1] <= {pixbuff[1][1:15], pixbuff[5][0]};
			pixbuff[5] <= {pixbuff[5][1:15], 2'b00};
			pixbuff[2] <= {pixbuff[2][1:15], pixbuff[6][0]};
			pixbuff[6] <= {pixbuff[6][1:15], 2'b00};
			pixbuff[3] <= {pixbuff[3][1:15], pixbuff[7][0]};
			if (!(en[0] & ~pixels_valid_i))
				pixbuff[7] <= {pixbuff[7][1:15], 2'b00};
		end
		if (src_h_cnt[3:0] == 4'd14 && !next_h && tgt_h_cnt < 320-48) begin
			if (repeat_request_enable) repeat_request_enable <= 1'b0;
			/*else*/ field_request <= 1'b1;
		end
		if (tgt_h_cnt == 319) begin
			tgt_v_cnt <= tgt_v_cnt + 1;
			tgt_h_cnt <= 10'd0;
			line_processing <= 1'b0;
		end
		if (tgt_v_cnt == 287 && tgt_h_cnt == 319) begin
			tgt_v_cnt <= 10'd0;
			frame_processing <= 1'b0;
		end 
	end // if (frame_processing)
	if (pixels_valid_i) begin
		for (int i = 0; i < 16; i++) begin
			if (repeat_request_enable)
				pixbuff[{wpixbuffptr, pixcnt}][i] <= pixels_i[31-i*2 -: 2];
			else begin
				case (pixcnt[1:0])
					2'b00: begin
						pixbuff[4][i] <= pixels_i[31-i*2 -: 2];
					end
					2'b01: begin
						pixbuff[5][i] <= pixels_i[31-i*2 -: 2];
					end
					2'b10: begin
						if (i > 0)
							pixbuff[6][i-1] <= pixels_i[31-i*2 -: 2];
						else
							pixbuff[2][15-i] <= pixels_i[31-i*2 -: 2];
					end
					2'b11: begin
						if (i > 0)
							pixbuff[7][i-1] <= pixels_i[31-i*2 -: 2];
						else
							pixbuff[3][15-i] <= pixels_i[31-i*2 -: 2];
					end
				endcase
			end				
		end
		pixcnt <= pixcnt + 1;
		if (pixcnt == 2'd3) begin
			first_reload <= 1'b0;
			pixcnt <= 2'd0;
			wpixbuffptr <= 1'b1;
		end
	end
	if (!en_i) begin
		wpixbuffptr <= 1'b0;
		rpixbuffptr <= 1'b0;
		tgt_h_cnt <= 10'd0;
		tgt_v_cnt <= 10'd0;
		field_request <= 1'b0;
		frame_processing <= 1'b0;
		pixcnt <= 2'd0;
		line_processing <= 1'b0;
		repeat_request_enable <= 1'b0;
		first_reload <= 1'b0;
		line_processing_ff <= 1'b0;
	end
end // always @(posedge clk_i)

logic [1:0] j_2b;
int i_x, base_i_x;
logic rpixbuffptr_cur;
int ibordered, jbordered;
always @(*) begin
	for (int j = 0; j < 4; j++) begin : wbbuf_gen_y
		for (int i = 0; i < 4; i++) begin : wbbuf_gen_x
			j_2b = j;
			wb_pixels[j*4+i] = pixbuff[{1'b0, j_2b}][i];
		end // wbbuf_gen_x
	end
end // always @(*)

genvar i, j;
generate
	for (j = 0; j < 4; j++) begin : color_gen_y
		for (i = 0; i < 4; i++) begin : color_gen_x
			cmixer cmixer_inst(
				  /*input */       .clk_i(clk_i)
				, /*input */       .nrst_i(nrst_i)
				, /*input [7:0] */ .bg_r_i(bg_r_i)
				, /*input [7:0] */ .bg_g_i(bg_g_i)
				, /*input [7:0] */ .bg_b_i(bg_b_i)
				, /*input [7:0] */ .c_r_i(c_r_i)
				, /*input [7:0] */ .c_g_i(c_g_i)
				, /*input [7:0] */ .c_b_i(c_b_i)
				, /*input [1:0] */ .p_i(wb_pixels[j*4+i])
				, /*output [7:0]*/ .p_r_o(base_vector[j][i][2])
				, /*output [7:0]*/ .p_g_o(base_vector[j][i][1])
				, /*output [7:0]*/ .p_b_o(base_vector[j][i][0])
			);
		end // color_gen_x
	end // color_gen
endgenerate

bicubic_alg_core #(1'b1) bicubic_alg_core_inst (
	  /*input*/ .clk_i(clk_i)
	, /*input*/ .nrst_i(nrst_i)
	, /*input [3:0] [11:0]*/ .x_rel_i(~tgt_h_cnt[0])
	, /*input [3:0] [11:0]*/ .y_rel_i(~tgt_v_cnt[0])
	, /*input [3:0] [3:0] [2:0] [7:0]*/ .base_vector_i(base_vector)
	, /*input*/ .en_i(line_processing_ff)
	, /*output [2:0] [7:0]*/ .pix_o(pixels_color_o)
	, /*output*/ .valid_o(pixels_color_valid_o)
);

endmodule // bicubic_data_manager
