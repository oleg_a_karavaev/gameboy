// `define DEBUG
module GB_sync_gen (
	  input clock20_i
	, input nrst_i
	, output gb_clock_o
	, output gb_vsync_o
	, output gb_hsync_o
);

logic clock_8 = 1'b0;
logic clock_4 = 1'b0;

logic [1:0] clkdivcnt = 2'd0;
logic mod = 1'b0;

always @(posedge clock20_i) begin
	clock_8 <= 1'b0;
	if (clkdivcnt <= 2'd1) begin
		clock_8 <= 1'b1;
		if (mod)
			clkdivcnt <= 2'd3;
		else
			clkdivcnt <= 2'd2;
		mod <= ~mod;
	end
	else
		clkdivcnt <= clkdivcnt + 2'h3;
end

logic [17:0] vsync_cnt = 18'd0;
logic [2:0] vsync_mod = 3'd1;
logic gb_vsync = 1'b0;
logic frame_start = 1'b0;

always @(posedge clock_8) begin
	if (vsync_cnt <= 18'd1) begin
		gb_vsync <= 1'b1;
		if (vsync_mod[2])
			vsync_cnt <= 18'd133334;
		else
			vsync_cnt <= 18'd133333;
		vsync_mod <= {vsync_mod[1:0], vsync_mod[2]};
	end
	else
		vsync_cnt <= vsync_cnt + 18'h3ffff;
	if (vsync_mod[0] && vsync_cnt == 18'd132534)
		gb_vsync <= 1'b0;
	else if (!vsync_mod[0] && vsync_cnt == 18'd132533)
		gb_vsync <= 1'b0;
	frame_start <= 1'b0;
	if (vsync_mod[0] && vsync_cnt == 18'd133134)
		frame_start <= 1'b1;
	else if (!vsync_mod[0] && vsync_cnt == 18'd133133)
		frame_start <= 1'b1;
end

logic hsync_gen_en = 1'b0;
logic [9:0] hsync_cnt = 10'd0;
logic gb_hsync = 1'b0;
logic clock_4_en = 1'b0;
logic [7:0] hsync_num_cnt = 8'd0;

always @(posedge clock_8) begin
	if (frame_start) begin
		hsync_gen_en <= 1'b1;
		hsync_cnt <= 10'd0;
		// clock_4_en <= 1'b1;
		hsync_num_cnt <= 8'd0;
	end
	if (hsync_gen_en) begin
		clock_4_en <= 1'b0;
		if (hsync_cnt >= 10'd32 && hsync_cnt <= 10'd352) begin
			if (!hsync_cnt[0])
				clock_4_en <= 1'b1;
		end
		if (hsync_cnt == 10'd16)
			clock_4_en <= 1'b1;
/*		else
			clock_4_en <= 1'b0;*/
		hsync_cnt <= hsync_cnt + 1;
		if (hsync_cnt == 10'd799) begin
			hsync_num_cnt <= hsync_num_cnt + 1;
			if (hsync_num_cnt == 8'd143)
				hsync_gen_en <= 1'b0;
			hsync_cnt <= 10'd0;
		end
		if (hsync_cnt == 10'd0)
			gb_hsync <= 1'b1;
		if (hsync_cnt == 10'd31)
			gb_hsync <= 1'b0;
	end
end

always @(posedge clock_8) begin
	clock_4 <= 1'b0;
	if (clock_4_en)
		clock_4 <= 1'b1;
end

assign gb_vsync_o = gb_vsync;
assign gb_hsync_o = gb_hsync;
assign gb_clock_o = clock_4;

endmodule


module GB_data_gen (
	  input clock_i
	, input vsync_i
	, input hsync_i
	, output [1:0] pix_o
);

parameter size = 8'd20;
parameter step = 8'd2;

logic [7:0] vcnt = 8'd0;
logic [7:0] hcnt = 8'd0;
logic [7:0] x = 8'd0;
logic [7:0] y = 8'd0;
logic [7:0] xstep = step;
logic [7:0] ystep = step;
logic [1:0] pix = 2'd0;
logic xstep_sign = 1'b0;
logic ystep_sign = 1'b0;
logic [7:0] x_next, y_next;
assign x_next = xstep_sign ? x - xstep : x + xstep;
assign y_next = ystep_sign ? y - ystep : y + ystep;

always @(posedge clock_i) begin
	if (hsync_i) begin
		hcnt <= 8'd0;
		vcnt <= vcnt + 1;
		if (vsync_i) begin
			vcnt <= 8'd0;
			x <= x_next;
			y <= y_next;
			if (x < step + step)
				xstep_sign <= 1'b0;
			else if (x > 8'd159 - (size + step + step))
				xstep_sign <= 1'b1;
			if (y < step + step)
				ystep_sign <= 1'b0;
			else if (y > 8'd139 - (size + step + step))
				ystep_sign <= 1'b1;
		end
	end
	else begin
		hcnt <= hcnt + 1;
`ifdef DEBUG
		// if (hcnt <= 3)
		// 	pix <= 2'd3;
		// else
		// 	pix <= 2'd1;
		if (hcnt[5:0] < 6'h20)
			pix <= 2'b10;
		else
			pix <= 2'b01;
`else
		if (hcnt >= x && hcnt < x + size && vcnt >= y && vcnt < y + size)
			pix <= 2'd3;
		else begin
			if (hcnt[5:0] < 6'h20)
				pix[0] <= 1'b1;
			else
				pix[0] <= 1'b0;
			if (vcnt[5:0] < 6'h20)
				pix[1] <= 1'b1;
			else
				pix[1] <= 1'b0;
		end
`endif
		if (hcnt == 8'd165)
			pix <= 2'd0;
	end
end
		
assign pix_o = pix;

endmodule
