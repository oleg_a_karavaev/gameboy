`include "gb_pkg.sv"
import gb_pkg::*;

module gameboyframe_to_framebuffer_bridge (
	  input clk_i
	, input gb_pscl_i
	, input gb_clock_i
	, input gameboy_frame_t gameboy_frame_i
	, output packed16_gameboy_frame_t packed16_gameboy_frame_o
);

	logic [7:0] rdusedw, rdusedw_ff, wrusedw;
	always @(posedge clk_i)
		rdusedw_ff <= rdusedw;
		
	packed16_gameboy_frame_t packed16_gameboy_frame;
	assign packed16_gameboy_frame_o = packed16_gameboy_frame;
	logic rdusedw_diff;
	assign rdusedw_diff = |(rdusedw_ff ^ rdusedw);
	logic fifo_rd_start;
	assign fifo_rd_start = rdusedw == 159 && rdusedw_diff;
	
	logic fifo_rd;
	logic [7:0] rcnt;
	
	always @(posedge clk_i)
		if (fifo_rd_start) begin
			fifo_rd <= 1'b1;
			rcnt <= 0;
		end
		else if (fifo_rd) begin
			rcnt <= rcnt + 1;
			if (rcnt == 159) fifo_rd <= 1'b0;
		end
		
	logic [3:0] pix_cnt = 4'd0;
	logic [3:0] pix;
	logic [1:0] fifo_rd_data_valid = 2'd0;
	logic [15:0] sop_flags, sop_flags_net;
	logic [15:0] eop_flags, eop_flags_net;
	assign sop_flags_net = {sop_flags[14:0], pix[1]};
	assign eop_flags_net = {eop_flags[14:0], pix[0]};
	
	always @(posedge clk_i) begin
		fifo_rd_data_valid <= {fifo_rd_data_valid[0], fifo_rd};
		packed16_gameboy_frame.valid <= 1'b0;
		packed16_gameboy_frame.sop <= 1'b0;
		packed16_gameboy_frame.eop <= 1'b0;
		if (fifo_rd_data_valid[1]) begin
			pix_cnt <= pix_cnt + 1;
			packed16_gameboy_frame.pixels <= {packed16_gameboy_frame.pixels[29:0], pix[3:2]};
			packed16_gameboy_frame.valid <= pix_cnt == 4'd15;
			sop_flags <= sop_flags_net;
			eop_flags <= eop_flags_net;
			if (pix_cnt == 4'd15) begin
				// packed16_gameboy_frame.valid <= 1'b1;
				packed16_gameboy_frame.sop <= |sop_flags_net;
				packed16_gameboy_frame.eop <= |eop_flags_net;
			end
		end
	end
	
	logic rdempty, wrfull, rdfull, wrempty;

	logic valid = 1'b0;
	logic sop = 1'b0;
	always @(posedge gb_clock_i)
		 if (gb_pscl_i) begin
	 		valid <= gameboy_frame_i.valid;
	 		sop <= gameboy_frame_i.sop;
		 end
	
	tx_fifo #(
		  .size(256)
		, .behav(1)
		, .dwidth(4)
	) tx_fifo_inst (
		  .wclk_i(~gb_clock_i)                                                      // - клок записи
		, .we_i(valid & gb_pscl_i)                                 // - сигнал записи
		, .d_i({gameboy_frame_i.pixel, sop, gameboy_frame_i.eop})  // - вх данные
		, .rclk_i(clk_i)                                                           // - клок чтения
		, .re_i(fifo_rd)                                                           // - чигнал чтения
		, .q_o(pix)                                                                // - вых данные
		, .empty_o(rdempty)                                                        // - флаг "пусто"
		, .rdusedw(rdusedw)
	);
/*	
	fifo160x4 fifo160x4_inst(
		  .data({gameboy_frame_i.pixel, gameboy_frame_i.sop, gameboy_frame_i.eop})
		, .rdclk(clk_i)
		, .rdreq(fifo_rd)
		, .wrclk(gb_clock_i)
		, .wrreq(gameboy_frame_i.valid & gb_pscl_i)
		, .q(pix)
		, .rdempty(rdempty)
		, .rdusedw(rdusedw)
		, .wrfull(wrfull)
		, .rdfull(rdfull)
		, .wrempty(wrempty)
		, .wrusedw(wrusedw)
	);
		
*/

endmodule