module rgb_itf #(parameter data_latency = 3) (
	  input clk_i
	, input nrst_i
	, input frame_ready_i
	, input switch_off_i
	, input source_i
	, input valid_i
	, output req_o
	, output bicubic_req_o
	, output hactive_o
	, output vactive_o
	, output read_pixel_pscl_o
	, output repeat_line_enable_o
	, input [9:0] width_i
	, input [9:0] height_i
	, input [9:0] vbp_i
	, input [9:0] vfp_i
	, input [9:0] vsw_i
	, input [9:0] hbp_i
	, input [9:0] hfp_i
	, input [9:0] hsw_i
	/*, input [4:0] data_latency_i*/
	, input [23:0] rgb_i
	
// -- TO DO
	, output [14:0] line_length_o
	, output hsync_flag_o
	, output vsync_flag_o
// -------------------------------------
	, output vsync_o
	, output hsync_o
	, output [23:0] rgb_o
	, output enable_o
	, output dcx_o
	, output csx_o
	, output wrx_o
	, output vsync_interface_enable_o
);

	reg [9:0] hcnt, vcnt;
	reg en, vsync, hsync, req, enable;
	reg [31 : 0] vsync_d, hsync_d, enable_d;
	reg [23:0] rgb;
	
	assign line_length_o = width_i - hbp_i - hfp_i;
	
	wire vbp_s, vbp_e, hbp_s, hbp_e, vfp_s, vfp_e, hfp_s, hfp_e, hsw_s, hsw_e, vsw_s, vsw_e;
	wire hp;
	wire hactive, vactive;
	logic dcx, csx;
	logic vactive_bicubic;
	
	
	logic bicubic_req = 1'b0;
	logic [19:0] hactive_ff = 0;
	
	always @(negedge clk_i) begin
		hactive_ff <= {hactive_ff[18:0], hactive_o};
		bicubic_req <= hactive_ff[16] & ~hactive_ff[15];
	end
	
	assign bicubic_req_o = bicubic_req & vactive_bicubic;
	
	assign dcx_o = 1'b1; //dcx;
	assign csx_o = 1'b1; //csx;
	
// ================= FRAME SYNC BOARDS =================== //
	assign vsync_interface_enable_o = 1'b0; //(~source_i & ~switch_off_i) & frame_gen_en;
	wire en_common;
	assign en_common = en | (~source_i & ~switch_off_i);
	// HOR PERIOD
	assign hp = en_common && (hcnt == width_i-1);
	// SYNC
	assign vsw_s = en_common && (vcnt == 10'd0 && hcnt == 10'd0);
	assign vsw_e = hp && (vcnt == vsw_i-1);
	assign hsw_s = en_common && (hcnt == 10'd1);
	assign hsw_e = en_common && (hcnt == vsw_i);
	// BACK POARCH	
	assign vbp_s = en_common && (vcnt == 10'd0 && hcnt == 10'd1);
	assign vbp_e = hp && (vcnt == vbp_i);
	assign hbp_s = en_common && (hcnt == 10'd0);
	assign hbp_e = en_common && (hcnt == hbp_i);
	// FRONT POARCH
	assign vfp_s = hp && (vcnt == height_i-vfp_i);
	assign vfp_e = hp && (vcnt == height_i-1);
	assign hfp_s = en_common && (hcnt == width_i-hfp_i);
	assign hfp_e = en_common && (hcnt == width_i-1);
	// ACTIVE
	assign hactive = en_common && (hcnt > hbp_i && hcnt <= width_i-hfp_i);
	assign vactive = en_common && (vcnt > vbp_i && vcnt <= height_i-vfp_i);
	
	assign hactive_o = hactive;
	assign vactive_o = en_common && (vcnt > vbp_i + 96 && vcnt <= height_i-vfp_i - 96);
	assign vactive_bicubic = en_common && (vcnt > vbp_i + 94 && vcnt <= height_i-vfp_i - 97);
	assign repeat_line_enable_o = vcnt[0];
	assign read_pixel_pscl_o = ~hcnt[0];
// ====================== COUNTERS ======================= //
	always @(negedge clk_i or negedge nrst_i)
		if (~nrst_i) begin
			hcnt <= 0;
			vcnt <= 0;
		end
		else begin
			if (en_common) begin
				hcnt <= hcnt + 1;
				if (hcnt == width_i-1)
					hcnt <= 0;
				if (hp) begin
					vcnt <= vcnt + 1;
					if (vcnt == height_i-1)
						vcnt <= 0;
				end
			end
			else
				hcnt <= 0;
		end

// ==================== SYNC SIGNALS ===================== //
	always @(negedge clk_i or negedge nrst_i)
		if (~nrst_i) begin
			vsync <= 1;
			hsync <= 1;
			vsync_d <= 32'hffffffff;
			hsync_d <= 32'hffffffff;
		end
		else begin
			if (hsw_s)
				hsync <= 0;
			else if (hsw_e)
				hsync <= 1;
			if (vsw_s)
				vsync <= 0;
			else if (vsw_e)
				vsync <= 1;
			vsync_d <= {vsync_d[30:0], vsync};
			hsync_d <= {hsync_d[30:0], hsync};
		end
		
	// assign vsync_o = (data_latency == 0 || source_i == 0) ? vsync : vsync_d[data_latency-1];
	// assign hsync_o = (data_latency == 0 || source_i == 0) ? hsync : hsync_d[data_latency-1];
	assign vsync_o = (data_latency == 0) ? vsync : vsync_d[data_latency-1];
	assign hsync_o = (data_latency == 0) ? hsync : hsync_d[data_latency-1];
	
	
	assign hsync_flag_o = hsync_d[0] & ~hsync;
	assign vsync_flag_o = vsync_d[0] & ~vsync;
	
// ======================== EN =========================== //

//	always en <= 1;
	
	logic enable_net;
	assign enable_net = ~(hactive & vactive); //vactive_o ? enable & ~valid_i : (vactive ? ~req : 1'b1);

	always @(negedge clk_i or negedge nrst_i)
		if (~nrst_i) en <= 0;
		else if (switch_off_i) en <= 0;
		else if (frame_ready_i) en <= 1;

	always @(negedge clk_i or negedge nrst_i)
		if (~nrst_i) req <= 0;
		else if (~en) req <= 0;
		else if (hactive && vactive) req <= 1;
		else req <= 0;
	
	always @(negedge clk_i or negedge nrst_i)
		if (~nrst_i) begin
			enable <= 1;
			enable_d <= 32'hffffffff;
		end
		else begin
			enable <= ~valid_i;
			enable_d <= {enable_d[30:0], enable_net};
		end
	wire enable_src;
	// assign enable_src = data_latency == 0 ? enable_net : enable_d[data_latency-1];
	assign enable_src = enable_d[4];
	assign req_o = req;
	
// ======================= DATA ========================== //
	always @(negedge clk_i or negedge nrst_i)
		if (~nrst_i) rgb <= 0;
		else /*if (valid_i)*/ rgb <= rgb_i;
		
//	reg source;
//	
//	always @(posedge clk_i or negedge nrst_i)
//		if (~nrst_i) source <= 0;
//		else if (switch_off_i) source <= 0;
//		else if (frame_ready_i) source <= 1;
	
	
	// VSYNC INTERFACE
	logic [23:0] rgb_cnt;
	logic [9:0] widthcnt, heightcnt, wait_cnt;
	logic [6:0] widthcolorstep;
	logic frame_gen_en = 0;
	logic [9:0] width_brd, height_brd;
	assign width_brd = width_i - hbp_i - hfp_i;
	logic enable_vsync_itf = 1;
	logic send_cmd = 0;
	always @(posedge clk_i) begin
		if (!hsync) begin
			rgb_cnt <= 24'h000000;
			widthcnt <= 0;
			heightcnt <= 0;
			wait_cnt <= 0;
			frame_gen_en <= 0;
			widthcolorstep <= width_brd[9:3];
			enable_vsync_itf <= 1;
			send_cmd <= 0;
			csx <= 1;
			dcx <= 1;
		end
		else begin
			// enable_vsync_itf <= ~frame_gen_en;
			if (hactive & vactive) begin
				widthcnt <= widthcnt + 1;
				if (widthcnt[6:0] == widthcolorstep-1) rgb_cnt <= {rgb_cnt[19:0], rgb_cnt[23:20]};
				if (widthcnt < 105) rgb_cnt <= 24'hffffff;
				else if (widthcnt < 215) rgb_cnt <= 24'hffff00;
				else rgb_cnt <= 24'h000000;
			end
			// if (!frame_gen_en) begin
				// csx <= 1;
				// dcx <= 1;
				// rgb_cnt <= 0;
				// widthcnt <= widthcnt + 1;
				// if (widthcnt == width_i-1) begin
					// wait_cnt <= wait_cnt + 1;
					// if (wait_cnt == 1) begin
						// wait_cnt <= 0;
						// frame_gen_en <= 1;
						// send_cmd <= 1;
						// csx <= 0;
						// dcx <= 0;
						// rgb_cnt <= 24'h000460;
					// end
					// widthcnt <= 0;
				// end
			// end
			// else if (send_cmd) begin
				// dcx <= 1;
				// send_cmd <= 0;
				// rgb_cnt <= 24'h00FF00;
			// end
			// else begin
				// widthcnt <= widthcnt + 1;
				// if (widthcnt == width_brd - 1) begin
					// widthcnt <= 0;
					// rgb_cnt <= 24'h00FF00;
					// heightcnt <= heightcnt + 1;
					// if (heightcnt == height_i - vbp_i - vfp_i - 1) begin
						// frame_gen_en <= 0;
						// csx <= 1;
						// heightcnt <= 0;
					// end
				// end
				
			// end
		end
	end
	
	
	// always @(negedge clk_i or negedge nrst_i)
		// if (~nrst_i) rgb_cnt <= 0;
		// else rgb_cnt <= rgb_cnt + 1;
	assign wrx_o = 1'b1; //frame_gen_en ? ~clk_i : 1'b1;
	assign rgb_o = (!enable_src & vactive_o) ? rgb : 0;
	// assign enable_o = source_i ? enable_src : ~(hactive & vactive);
	assign enable_o = enable_src;
	
	
endmodule
