module dbi_itf (
	  input clk_i
	, input dbi_txbuf_wrclk_i
	, input nrst_i
	, input dbi_txbuf_wr_i
	, input dbi_cmd_wr_i 
	, input [1:0] input_data_size_i
	, input [7:0] cmd_i
	, input [31:0] lcd_dbi_txbuf_i
	, output [31:0] lcd_dbi_data_o
	, output lcd_dbi_data_valid_o
	, output [1:0] lcd_dbi_data_size_o
	, output lcd_itf_wr_busy_o
	, output lcd_itf_wr_ready_o
	, output lcd_itf_rd_busy_o
	, output lcd_itf_rd_ready_o
	, output cmdbuf_empty_o
	, output cmdbuf_full_o
	, output txbuf_empty_o
	, output txbuf_full_o
	, input vsync_i
	, input hsync_i
	, input valid_i
	, input [7:0] pix_r_i
	, input [7:0] pix_g_i
	, input [7:0] pix_b_i
	// LCD
	, output [15:0] db_io_o
	, output db_io_oe
	, input [15:0] db_io_i
//	, inout [15:0] db_io
	, output csx_o
	, output resx_o
	, output dcx_o
	, output wrx_o
	, output rdx_o
);

reg [255:0] [7:0] dbi_txbuf;
reg [7:0] dbi_txbuf_waddr, dbi_txbuf_raddr;

reg [15:0] [12:0] dbi_cmdbuf;
reg [3:0] dbi_cmdbuf_waddr, dbi_cmdbuf_raddr;

reg [15:0] db_o;
reg csx;
reg resx;
reg dcx;
reg wrx;
reg rdx;
reg db_oe;
reg break_frame;
reg lcd_itf_wr_busy, lcd_itf_rd_busy;
reg [23:0] lcd_dbi_rdata;
reg lcd_dbi_rdata_valid;
reg [1:0] lcd_dbi_data_size;
reg rd_dummy_released;

reg [3:0] dbi_state;
localparam dbi_idle = 4'd0;
localparam dbi_wr_cmd = 4'd1;
localparam dbi_wr_dummy = 4'd2;
localparam dbi_wr = 4'd3;
localparam dbi_rd_cmd = 4'd4;
localparam dbi_rd_dummy = 4'd5;
localparam dbi_rd = 4'd6;
localparam dbi_wr_precharge = 4'd7;
localparam dbi_rd_precharge = 4'd8;
localparam dbi_wr_frame = 4'd9;

assign wrx_o = dbi_state == dbi_wr_frame ? ~clk_i : wrx;
assign csx_o = csx;
assign resx_o = nrst_i;
assign dcx_o = dcx;
assign rdx_o = rdx;
assign db_io_o = db_o;
assign db_io_oe = db_oe;


assign lcd_dbi_data_o = {16'd0, lcd_dbi_rdata};
assign lcd_dbi_data_valid_o = lcd_dbi_rdata_valid;
assign lcd_dbi_data_size_o = lcd_dbi_data_size;
assign lcd_itf_wr_busy_o = lcd_itf_wr_busy;
assign lcd_itf_wr_ready_o = ~lcd_itf_wr_busy;
assign lcd_itf_rd_busy_o = lcd_itf_rd_busy;
assign lcd_itf_rd_ready_o = ~lcd_itf_rd_busy;

wire need_rd_dummy, cmdbuf_empty, itf_free, cur_cmd_is_read, cmdbuf_full, txbuf_empty, txbuf_full;

assign cmdbuf_empty_o = cmdbuf_empty;
assign cmdbuf_full_o = cmdbuf_full;
assign txbuf_empty_o = txbuf_empty;
assign txbuf_full_o = txbuf_full;

assign itf_free = !valid_i;

assign cur_cmd_is_read = 
	  dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h04
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h05
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h09
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h0a
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h0b
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h0c
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h0d
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h0e
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h0f
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h2e
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h3e
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h45
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h52
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h54
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h56
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h5f
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h68
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'hda
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'hdb
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'hdc
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'hd2
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'hd3
	| dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'hd8
;

assign cmdbuf_empty = dbi_cmdbuf_waddr == dbi_cmdbuf_raddr;
assign cmdbuf_full = dbi_cmdbuf_waddr - dbi_cmdbuf_raddr >= 15;
assign txbuf_empty = dbi_txbuf_waddr == dbi_txbuf_raddr;
assign txbuf_full = dbi_txbuf_waddr - dbi_txbuf_raddr >= 254;

assign need_rd_dummy =
	  cmd_i == 8'h04
	| cmd_i == 8'h05
	| cmd_i == 8'h09
	| cmd_i == 8'h0a
	| cmd_i == 8'h0b
	| cmd_i == 8'h0c
	| cmd_i == 8'h0d
	| cmd_i == 8'h0e
	| cmd_i == 8'h0f
	| cmd_i == 8'h2e
	| cmd_i == 8'h3e
	| cmd_i == 8'h45
	| cmd_i == 8'h52
	| cmd_i == 8'h54
	| cmd_i == 8'h56
	| cmd_i == 8'h5f
	| cmd_i == 8'h68
	| cmd_i == 8'hda
	| cmd_i == 8'hdb
	| cmd_i == 8'hdc
	| cmd_i == 8'hd2
	| cmd_i == 8'hd3
	| cmd_i == 8'hd8
;


wire [3:0] payload_byte_size;

assign payload_byte_size = 
	  ((cmd_i == 8'h04 | cmd_i == 8'hb6 | cmd_i == 8'hd1 | cmd_i == 8'hd3 | cmd_i == 8'he2 | cmd_i == 8'he3) ? 4'd3
	: ((cmd_i == 8'hb1 | cmd_i == 8'hb2 | cmd_i == 8'hb3 | cmd_i == 8'hbe | cmd_i == 8'hc0 | cmd_i == 8'hc6 | cmd_i == 8'hd0 | cmd_i == 8'hd2 | cmd_i == 8'hd8 | cmd_i == 8'hfc | cmd_i == 8'hff) ? 4'd2
	: ((cmd_i == 8'h09 | cmd_i == 8'h2a | cmd_i == 8'h2b | cmd_i == 8'h30 | cmd_i == 8'hb5 | cmd_i == 8'hc5 | cmd_i == 8'hf7) ? 4'd4
	: ((cmd_i == 8'h33) ? 4'd6
	: ((cmd_i == 8'h2c | cmd_i == 8'h3c | cmd_i == 8'h2e | cmd_i == 8'h3e | cmd_i == 8'h00) ? 4'd0
	: ((cmd_i == 8'hb9 | cmd_i == 8'hba) ? 4'd12
	: ((cmd_i == 8'hf2) ? 4'd11
	: ((cmd_i == 8'he0 | cmd_i == 8'he1) ? 4'd15
	: 4'd1 ))))))))
;

wire cmd_is_nop;
assign cmd_is_nop = dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'd0;

always @(negedge nrst_i or posedge dbi_txbuf_wrclk_i)
	if (~nrst_i) begin //сброс в значения по умолчанию
		dbi_txbuf_waddr <= 0;
		dbi_txbuf <= {256{8'd0}};
		dbi_cmdbuf_waddr <= 0;
		dbi_cmdbuf <= {16{13'd0}};
	end
	else begin
		if (dbi_cmd_wr_i) begin
			dbi_cmdbuf[dbi_cmdbuf_waddr] <= {need_rd_dummy, payload_byte_size, cmd_i};
			dbi_cmdbuf_waddr <= dbi_cmdbuf_waddr + 1;
		end
		if (dbi_txbuf_wr_i) begin
			dbi_txbuf[dbi_txbuf_waddr] <= lcd_dbi_txbuf_i[7:0];
			dbi_txbuf_waddr <= dbi_txbuf_waddr + 1;
			if (|input_data_size_i) begin
				dbi_txbuf[dbi_txbuf_waddr+1] <= lcd_dbi_txbuf_i[15:8];
				dbi_txbuf_waddr <= dbi_txbuf_waddr + 2;
			end
			if (input_data_size_i[1]) begin
				dbi_txbuf[dbi_txbuf_waddr+2] <= lcd_dbi_txbuf_i[23:16];
				dbi_txbuf_waddr <= 3;
			end
			if (&input_data_size_i) begin
				dbi_txbuf[dbi_txbuf_waddr+3] <= lcd_dbi_txbuf_i[31:24];
				dbi_txbuf_waddr <= dbi_txbuf_waddr + 4;
			end
		end
//		if (dbi_txbuf_waddr_rst)
//			dbi_txbuf_waddr <= 0;
	end
	

reg [6:0] dbi_cnt_border;

reg [3:0] dbitxcnt, dbirxcnt;

always @(posedge clk_i or negedge nrst_i)
	if (~nrst_i) begin
		dbi_state <= dbi_idle;
		dbitxcnt <= 0;
		dbirxcnt <= 0;
		dbi_txbuf_raddr <= 0;
		dbi_cmdbuf_raddr <= 0;
		rd_dummy_released <= 1'b0;
		lcd_dbi_rdata_valid <= 1'b0;
		lcd_dbi_rdata <= 0;
		db_oe <= 1'b0;
		csx <= 1'b1;
		db_o <= 0;
		dcx <= 1'b1;
		wrx <= 1'b1;
		rdx <= 1'b1;
		break_frame <= 0;
		lcd_itf_wr_busy <= 0;
		lcd_itf_rd_busy <= 0;
		lcd_dbi_data_size <= 0;
	end
	else begin
		case (dbi_state)
			dbi_idle: begin
					rd_dummy_released <= 1'b0;
					lcd_dbi_rdata_valid <= 1'b0;
					csx <= 1'b1;
					db_oe <= 1'b0;
					dcx <= 1'b1;
					wrx <= 1'b1;
					rdx <= 1'b1;
					if (itf_free && !cmdbuf_empty) begin
						break_frame <= 1'b1;
						db_oe <= 1'b1;
						csx <= 1'b0;
						dcx <= 1'b0;
						wrx <= 1'b0;
						db_o <= dbi_cmdbuf[dbi_cmdbuf_raddr][7:0];
						if (!cur_cmd_is_read) begin
							dbi_state <= dbi_wr_cmd;
							dbitxcnt <= dbi_cmdbuf[dbi_cmdbuf_raddr][11:8];
							lcd_itf_wr_busy <= 1'b1;
						end
						else begin
							dbi_state <= dbi_rd_cmd;
							dbirxcnt <= dbi_cmdbuf[dbi_cmdbuf_raddr][11:8];
							lcd_itf_rd_busy <= 1'b1;
						end
					end
					if (hsync_i) begin
						if (break_frame)
							db_o <= 8'h3c;
						else
							db_o <= 8'h2c;
						db_oe <= 1'b1;
						csx <= 1'b0;
						dcx <= 1'b0;
						wrx <= 1'b0;
						dbi_state <= dbi_wr_cmd;
						lcd_itf_wr_busy <= 1'b1;
					end
					if (vsync_i) begin
						break_frame <= 1'b0;
						db_o <= 8'h2c;
						db_oe <= 1'b1;
						csx <= 1'b0;
						dcx <= 1'b0;
						wrx <= 1'b0;
						dbi_state <= dbi_wr_cmd;
						lcd_itf_wr_busy <= 1'b1;
					end
				end
			dbi_wr_cmd: begin
					if (wrx) begin
						if (cmd_is_nop)
							dbi_state <= dbi_idle;
						else
							dbi_state <= dbi_wr_precharge;
						db_oe <= 1'b0;
						dcx <= 1'b1;
					end
					else
						wrx <= 1'b1;
				end
			dbi_wr_frame: begin
					if (!valid_i)
						dbi_state <= dbi_idle;
					db_o <= {pix_r_i[7:3], pix_g_i[7:2], pix_b_i[7:3]};
				end
			dbi_wr_precharge: begin
					if (valid_i) begin
						dbi_state <= dbi_wr_frame;
						db_o <= {pix_r_i[7:3], pix_g_i[7:2], pix_b_i[7:3]};
					end
					else begin
						if (dbitxcnt == 0 && !cmdbuf_empty) begin
							dbi_state <= dbi_idle;
						end
						else begin
							dbi_state <= dbi_wr;
							db_oe <= 1'b1;
							wrx <= 1'b0;
							db_o <= {8'd0, dbi_txbuf[dbi_txbuf_raddr+0]};
							dbi_txbuf_raddr <= dbi_txbuf_raddr + 1;
						end
					end
				end
			dbi_wr: begin
					if (wrx) begin
						if (dbitxcnt == 0) begin
							dbi_state <= dbi_wr_precharge;
						end
						else begin
							if (dbitxcnt == 1) begin
								dbi_state <= dbi_idle;
								dbi_cmdbuf_raddr <= dbi_cmdbuf_raddr + 1;
								lcd_itf_wr_busy <= 1'b0;
							end
							else
								dbi_state <= dbi_wr_precharge;
							dbitxcnt <= dbitxcnt + 4'hf;
						end
						db_oe <= 1'b0;
						dcx <= 1'b1;
					end
					else
						wrx <= 1'b1;
				end
			dbi_rd_cmd: begin
					if (wrx) begin
						dbi_state <= dbi_rd_precharge;
						rd_dummy_released <= 1'b0;
						db_oe <= 1'b0;
						dcx <= 1'b1;
					end
					else
						wrx <= 1'b1;
				end
			dbi_rd_precharge: begin
					lcd_dbi_rdata_valid <= 1'b0;
					if (dbirxcnt == 0 && !cmdbuf_empty) begin
						dbi_state <= dbi_idle;
					end
					else begin
						if (dbi_cmdbuf[dbi_cmdbuf_raddr][12] && rd_dummy_released)
							dbi_state <= dbi_rd_dummy;
						else
							dbi_state <= dbi_rd;
						rdx <= 1'b0;
					end
				end
			dbi_rd_dummy: begin
					if (rdx) begin
						dbi_state <= dbi_rd_precharge;
						rd_dummy_released <= 1'b1;
					end
					else
						rdx <= 1'b1;
				end
			dbi_rd: begin
					if (rdx) begin
						lcd_dbi_rdata <= db_io_i;
						lcd_dbi_rdata_valid <= 1'b1;
						if (dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h2e | dbi_cmdbuf[dbi_cmdbuf_raddr][7:0] == 8'h3e)
							lcd_dbi_data_size <= 2'b01;
						else
							lcd_dbi_data_size <= 2'b00;
						rd_dummy_released <= 1'b1;
						if (dbirxcnt == 0) begin
							dbi_state <= dbi_rd_precharge;
						end
						else begin
							if (dbirxcnt == 1) begin
								dbi_state <= dbi_idle;
								dbi_cmdbuf_raddr <= dbi_cmdbuf_raddr + 1;
								lcd_itf_rd_busy <= 1'b0;
							end
							else
								dbi_state <= dbi_rd_precharge;
							dbirxcnt <= dbirxcnt + 4'hf;
						end
					end
					else
						rdx <= 1'b1;
				end
			default:
				dbi_state <= dbi_idle;
		endcase
	end
	
endmodule
