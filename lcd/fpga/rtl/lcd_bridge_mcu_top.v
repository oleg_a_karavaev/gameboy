`timescale 1 ps / 1 ps
module lcd_bridge_mcu_top #(
	parameter reg debug_version = 1'b0
)
(
	input  wire        clk_clk,                         //                      clk.clk
	output wire        dc_export,                       //                       dc.export
	output wire        lcd_bridge_0_pixclock_clk,       //    lcd_bridge_0_pixclock.clk
	output wire [15:0] lcd_bridge_0_rgb_rgb_o,          //         lcd_bridge_0_rgb.rgb_o
	output wire        lcd_bridge_0_rgb_vsync_o,        //                         .vsync_o
	output wire        lcd_bridge_0_rgb_hsync_o,        //                         .hsync_o
	output wire        lcd_bridge_0_rgb_enable_o,       //                         .enable_o
	input  wire [1:0]  lcd_bridge_0_wb_pix_i,           //          lcd_bridge_0_wb.pix_i
	input  wire        lcd_bridge_0_wb_vsync_i,         //                         .vsync_i
	input  wire        lcd_bridge_0_wb_hsync_i,         //                         .hsync_i
	input  wire        miso_export,                     //                     miso.export
	output wire        mosi_export, // mosi_external_connection.export
//	input  wire        reset_reset_n,                   //                    reset.reset_n
	output wire        rst_export,                      //                      rst.export
	output wire        sck_export,                      //                      sck.export
	output wire        ss_export,                        //                       ss.export
	input  wire        wb_clock_clk                     //                 wb_clock.clk
);

	wire lcd_bridge_0_mcu_itf_dcx, lcd_bridge_0_mcu_itf_csx, lcd_bridge_0_mcu_itf_wrx, lcd_bridge_0_select_sel;
	wire ss_export_local, sck_export_local, dc_export_local;
	
	// assign ss_export = lcd_bridge_0_select_sel ? lcd_bridge_0_mcu_itf_csx : ss_export_local;
	// assign sck_export = lcd_bridge_0_select_sel ? lcd_bridge_0_mcu_itf_wrx : sck_export_local;
	// assign dc_export = lcd_bridge_0_select_sel ? lcd_bridge_0_mcu_itf_dcx : dc_export_local;

	wire pll_clock;
	reg [3:0] rst_cnt = 4'd0;
	always @(posedge clk_clk)
		if (!rst_cnt[3]) rst_cnt <= {rst_cnt[2:0], 1'b1};
		
	wire [1:0]  wb_pix;
	wire wb_vsync;
	wire wb_hsync;
	wire wb_clock;
	wire locked;
	
	pll50to20 pll50to20_inst(
		.areset(~rst_cnt[3]),
		.inclk0(clk_clk),
		.c0(pll_clock),
		.locked(locked));
generate
	if (debug_version) begin
		GB_sync_gen GB_sync_gen_inst(
			  .clock20_i(pll_clock)
			, .nrst_i(locked)
			, .gb_clock_o(wb_clock)
			, .gb_vsync_o(wb_vsync)
			, .gb_hsync_o(wb_hsync)
		);
		
		GB_data_gen GB_data_gen_inst(
			  .clock_i(wb_clock)
			, .vsync_i(wb_vsync)
			, .hsync_i(wb_hsync)
			, .pix_o(wb_pix)
		);
	end
	else begin
		assign wb_clock = wb_clock_clk;
		assign wb_vsync = lcd_bridge_0_wb_vsync_i;
		assign wb_hsync = lcd_bridge_0_wb_hsync_i;
		assign wb_pix = lcd_bridge_0_wb_pix_i;
	end
endgenerate
	
	
	gb db_inst(
		.clk_clk(clk_clk),                         //                      clk.clk
		.clk_0_clk(pll_clock),
		.dc_export(dc_export),                       //                       dc.export
		/*.lcd_bridge_0_mcu_itf_dcx(lcd_bridge_0_mcu_itf_dcx),        //     lcd_bridge_0_mcu_itf.dcx
		.lcd_bridge_0_mcu_itf_csx(lcd_bridge_0_mcu_itf_csx),        //                         .csx
		.lcd_bridge_0_mcu_itf_wrx(lcd_bridge_0_mcu_itf_wrx),        //                         .wrx
		*/.lcd_bridge_0_rgbclk_clk(lcd_bridge_0_pixclock_clk),       //    lcd_bridge_0_pixclock.clk
		.lcd_bridge_0_rbg_rgb(lcd_bridge_0_rgb_rgb_o),          //         lcd_bridge_0_rgb.rgb_o
		.lcd_bridge_0_rbg_vsync(lcd_bridge_0_rgb_vsync_o),        //                         .vsync_o
		.lcd_bridge_0_rbg_hsync(lcd_bridge_0_rgb_hsync_o),        //                         .hsync_o
		.lcd_bridge_0_rbg_enable(lcd_bridge_0_rgb_enable_o),       //                         .enable_o
		//.lcd_bridge_0_select_sel(lcd_bridge_0_select_sel),         //      lcd_bridge_0_select.sel
		.lcd_bridge_0_wblcd_pix(wb_pix), //lcd_bridge_0_wb_pix_i),           //          lcd_bridge_0_wb.pix_i
		.lcd_bridge_0_wblcd_vsync(wb_vsync), //lcd_bridge_0_wb_vsync_i),         //                         .vsync_i
		.lcd_bridge_0_wblcd_hsync(wb_hsync), //lcd_bridge_0_wb_hsync_i),         //                         .hsync_i
		.miso_export(miso_export),                     //                     miso.export
		.mosi_export(mosi_export), // mosi_external_connection.export
		.reset_reset_n(locked),                   //                    reset.reset_n
		.rst_export(rst_export),                      //                      rst.export
		.sck_export(sck_export),                      //                      sck.export
		.ss_export(ss_export),                       //                       ss.export
		.clk_1_clk(wb_clock) //wb_clock_clk),                     //                 wb_clock.clk
//		.lcd_bridge_0_pll_clock_out_clk(pll_clock)
	);
	
endmodule
