`include "gb_pkg.sv"
import gb_pkg::*;

module frame_buffer_control (
	  input clk_i
	, input packed16_gameboy_frame_t packed16_gameboy_frame_i
	, output [1:0] pixels_o
	, output [31:0] pixels_bc_o
	, input hactive_i
	, input vactive_i
	, input pscl_i
	, output buffer_ready_o
	, input repeat_line_enable_i
	, output pixels_valid_o
	, input bicubic_mode_en_i
	, input switch_to_next_line_en_i
	, input req_field_i
	, input repeat_request_enable_i
	, input bc_to_next_line_enable_i
	, input bc_frame_start_i
	// RAM itf
	, output [31:0] ram_wdata_o
	, output [10:0] ram_waddr_o
	, output [2:0] ram_we_o
	, input [31:0] ram_rdata_i
	, output [10:0] ram_raddr_o
	, output [2:0] ram_re_o
);

	logic [1:0] ram_wptr = 2'd0;
	logic [1:0] ram_rptr = 2'd0;
	
	logic [31:0] ram_wdata;
	logic [10:0] ram_waddr = 11'd0;
	logic [2:0] ram_we = 3'd0;
	logic [10:0] ram_raddr = 11'd0;
	logic [10:0] ram_raddr_base = 11'd0;
	logic [10:0] ram_raddr_lineoffs = 11'd0;
	logic [2:0] ram_re = 3'd0;
	logic [31:0] pixels;
	logic buffer_ready = 1'b0;
	logic bicubic_reading = 1'b0;
	logic bicubic_reading_ff = 1'b0;
	logic [2:0] readcnt = 2'd0;

	assign buffer_ready_o = buffer_ready;
	assign ram_wdata_o = ram_wdata;
	assign ram_waddr_o = ram_waddr;
	assign ram_raddr_o = ram_raddr;
	assign ram_we_o = ram_we;
	assign ram_re_o = ram_re;
	assign pixels_o = pixels[31:30];
	assign pixels_bc_o = pixels;
	logic vactive_ff = 1'b0;
	logic [3:0] hactive_ff = 1'b0;
	
	logic second_line_read = 1'b0;
	logic [15:0] pixels_rd = 16'd0;
	logic pscl_ff;
	logic pixels_valid = 1'b0;
	logic [1:0] pixels_valid_ff = 2'b00;
	assign pixels_valid_o = pixels_valid_ff[1];
	logic repeat_line_enable_ff = 1'b0;
	
	logic repeat_request_enable = 1'b0;
	logic input_frame_data_valid_ff = 1'b0;
	logic input_frame_eop_ff = 1'b0;

	logic mode_switch_clr;

	logic bicubic_mode_en_ff = 1'b0;
	assign mode_switch_clr = bicubic_mode_en_i ^ bicubic_mode_en_ff;

	always @(posedge clk_i) begin
		bicubic_mode_en_ff <= bicubic_mode_en_i;
		vactive_ff <= vactive_i;
		hactive_ff <= {hactive_ff[2:0], hactive_i};
		pixels_valid_ff <= {pixels_valid_ff[0], pixels_valid};
		repeat_line_enable_ff <= repeat_line_enable_i;
		bicubic_reading_ff <= bicubic_reading;
		pscl_ff <= pscl_i;
		pixels_valid <= 1'b0;
		input_frame_data_valid_ff <= packed16_gameboy_frame_i.valid;
		ram_we <= 0;
		input_frame_eop_ff <= packed16_gameboy_frame_i.eop;
		if (input_frame_data_valid_ff)
			ram_waddr <= ram_waddr + 1;
		if (input_frame_eop_ff)
			ram_waddr <= 11'd0;
		if (packed16_gameboy_frame_i.valid) begin
			ram_wdata <= packed16_gameboy_frame_i.pixels;
			ram_we[ram_wptr] <= 1'b1;
		end
		if (packed16_gameboy_frame_i.eop) begin
			buffer_ready <= 1'b1;
			if (!((ram_wptr == ram_rptr - 1 && ram_rptr > ram_wptr) || (ram_wptr == 2'd2 && ram_rptr == 2'd0))) begin
				ram_wptr <= ram_wptr + 1;
				if (ram_wptr == 2'd2) ram_wptr <= 2'd0;
			end
		end
		if (bicubic_mode_en_i) begin
			if (req_field_i) begin
				if (repeat_request_enable_i) repeat_request_enable <= 1'b1;
				if (!bicubic_reading) begin
					pixels_rd <= 16'd1;
					bicubic_reading <= 1'b1;
				end // if (!bicubic_reading)
			end // if (req_field_i)
//			if (pscl_ff) begin
				pixels_rd <= {pixels_rd[14:0], pixels_rd[15]};
				if (bicubic_reading) begin
					if (ram_re[ram_rptr])
						readcnt <= readcnt + 1;
					pixels_valid <= 1'b1;
					ram_re[ram_rptr] <= 1'b1;
					ram_raddr <= ram_raddr_base + ram_raddr_lineoffs; //11'd10;
					ram_raddr_lineoffs <= ram_raddr_lineoffs + 11'd10;
					if (repeat_request_enable) begin
						if (readcnt[1:0] == 2'd2) begin
							ram_raddr_base <= ram_raddr_base + 1;
							// if (first_line)
							// 	ram_raddr_lineoffs <= 11'd0;
							// else
							if (ram_raddr_lineoffs != 11'd0) begin
								ram_raddr_lineoffs <= ram_raddr_lineoffs - 11'd30;
							end
						end
					end
					if (readcnt[1:0] == 2'd3) begin
						if (!repeat_request_enable) begin
							bicubic_reading <= 1'b0;
							ram_re[ram_rptr] <= 1'b0;
							pixels_valid <= 1'b0;
							ram_raddr_base <= ram_raddr_base + 1;
							if (switch_to_next_line_en_i && bc_to_next_line_enable_i && ram_raddr_base == 11'd9)
								ram_raddr_lineoffs <= ram_raddr_lineoffs - 11'd30;
							else
								ram_raddr_lineoffs <= ram_raddr_lineoffs - 11'd40;
							if (ram_raddr_base == 11'd9) begin
								ram_raddr_base <= 11'd0;
	// 							if (switch_to_next_line_en_i) begin
	// 								if (repeat_request_enable)
	// 									ram_raddr_lineoffs <= ram_raddr_lineoffs - 11'd70;
	// 								else
	// 									ram_raddr_lineoffs <= ram_raddr_lineoffs - 11'd30;
	// 							end
	// //							else
	// //								ram_raddr_lineoffs <= ram_raddr_lineoffs - 11'd80;
							end
						end
						repeat_request_enable <= 1'b0;
					end
////					if (repeat_request_enable && readcnt == 2'd2)
////						ram_raddr_lineoffs <= 11'd0;
				end
				if (bicubic_reading_ff) pixels <= ram_rdata_i;
//			end
			if (bc_frame_start_i) begin
				ram_raddr_base <= 0;
				ram_raddr_lineoffs <= 0;
				ram_raddr <= 0;
			end
		end // if (bicubic_mode_en_i)
		else if ((hactive_i | hactive_ff[2]) && vactive_i) begin
			if (pscl_ff) begin
				pixels_valid <= 1'b1;
				pixels <= {pixels[29:0], 2'b00};
				pixels_rd <= {pixels_rd[14:0], pixels_rd[15]};
				if (pixels_rd[0]) begin
				// if (pscl_i) begin
					ram_re[ram_rptr] <= 1'b1;
					// ram_raddr <= ram_raddr + 1;
				end
				if (pixels_rd[1]) begin
					ram_raddr <= ram_raddr + 1;
				end
				if (pixels_rd[1]) pixels <= ram_rdata_i;
			end
			if (!(hactive_ff[0] | hactive_ff[2])) pixels_rd <= 16'd1;
		end
		else
			pixels <= 0;
		if (repeat_line_enable_i && !repeat_line_enable_ff) begin
			if (vactive_i) begin
				ram_raddr <= ram_raddr - 11'd10;
			end
		end
		if (bicubic_mode_en_i) begin
			if (bc_frame_start_i) begin
				ram_raddr <= 11'd0;
				ram_rptr <= ram_wptr - 1;
				if (ram_wptr == 2'd0) ram_rptr <= 2'd2;
				ram_raddr_base <= 11'd0;
			end
		end
		else begin
			if (vactive_i && !vactive_ff) begin
				ram_raddr <= 11'd0;
				ram_rptr <= ram_wptr - 1;
				if (ram_wptr == 2'd0) ram_rptr <= 2'd2;
				ram_raddr_base <= 11'd0;
			end
		end
		if (mode_switch_clr) begin
			ram_raddr_base <= 0;
			ram_raddr_lineoffs <= 0;
			ram_raddr <= 0;
			ram_rptr <= ram_wptr - 1;
			if (ram_wptr == 2'd0) ram_rptr <= 2'd2;
		end
	end

endmodule
	
