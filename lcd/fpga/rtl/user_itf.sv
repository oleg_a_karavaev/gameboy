typedef struct {
	logic device_reset;
	logic frame_buffer_enable;
	logic bicubic_enable;
	logic tee_en;
	logic en;
	logic source;
	logic load_ctrls;
	logic clr;
} ctl_t;

typedef struct {
	logic[15:0] width;
	logic[15:0] height;
} active_size_t;

typedef struct {
	logic[9:0] bpw;
	logic[9:0] fpw;
	logic[9:0] sw;
} sync_length_t;

typedef struct packed{
	logic[7:0] red;
	logic[7:0] green;
	logic[7:0] blue;
} color_t;

typedef struct {
	logic frame_overflow;
	logic frame_underflow;
} status_t;	


typedef struct {
	ctl_t ctl;
	color_t background;
	color_t color;
	active_size_t frame_size;
	sync_length_t sync_length_h;
	sync_length_t sync_length_v;
	status_t status;
} reg_t;

// Блок пользовательского интерфейса. pllsynth.pdf-6, p.22
module user_itf (
	  input clk_i
	, input nrst_i
	, input [4:0] address_i
	, input [31:0] writedata_i
	, input read_i
	, input write_i
	, output reg [31:0] readdata_o
	, output waitrequest_o
	, output [1:0] response_o
	// internal ctrl
	, output anrst_o
	, output frame_buffer_en_o
	, output bicubic_en_o
	, output [2:0] [7:0] bg_o
	, output [2:0] [7:0] cl_o
	, output [9:0] width_o
	, output [9:0] height_o
	, output [9:0] vbp_o
	, output [9:0] vfp_o
	, output [9:0] vsw_o
	, output [9:0] hbp_o
	, output [9:0] hfp_o
	, output [9:0] hsw_o
//	, output [9:0] data_latency_o
	, output switch_off_o
	, output source_o
	, input frame_overflow_i
	, input frame_underflow_i
);

// Avalon-MM

logic release_transaction;
reg_t r, rbuf;

assign waitrequest_o = (read_i | write_i) & ~(release_transaction);

logic [31:0] anrst;
assign anrst_o = anrst[31];
assign response_o = 2'b00;
always @(posedge clk_i or negedge nrst_i) begin
	if (~nrst_i) begin
		r.ctl.device_reset <= 0;
		r.ctl.frame_buffer_enable <= 0;
		r.ctl.bicubic_enable <= 0;
		r.ctl.tee_en <= 0;
		r.ctl.en <= 0;
		r.ctl.load_ctrls <= 0;
		r.ctl.clr <= 0;
		r.ctl.source <= 0;
		r.background.red <= 8'h00;
		r.background.green <= 8'h00;
		r.background.blue <= 8'h00;
		r.color.red <= 8'hff;
		r.color.green <= 8'hff;
		r.color.blue <= 8'hff;
		r.frame_size.width <= 16'd479;
		r.frame_size.height <= 16'd696;
		r.sync_length_h.bpw <= 10'd103;
		r.sync_length_h.fpw <= 10'd88;
		r.sync_length_h.sw <= 10'd15;
		r.sync_length_v.bpw <= 10'd198;
		r.sync_length_v.fpw <= 10'd178;
		r.sync_length_v.sw <= 10'd20;
		release_transaction <= 0;
		anrst <= 1;
	end
	else begin
		anrst <= {anrst[30:0], 1'b1};
		release_transaction <= (read_i | write_i);
		if (read_i && !release_transaction) begin
			readdata_o <= 0;
			case (address_i[4:0])
				5'h0: begin
					readdata_o[31] <= r.ctl.device_reset;
					readdata_o[8] <= r.ctl.en;
					readdata_o[9] <= r.ctl.source;
					readdata_o[30] <= r.ctl.frame_buffer_enable;
					readdata_o[29] <= r.ctl.bicubic_enable;
					readdata_o[28] <= r.ctl.tee_en;
				end
				5'h4: begin
					readdata_o[23:0] <= {r.background.red, r.background.green, r.background.blue};
				end
				5'h8: begin
					readdata_o[23:0] <= {r.color.red, r.color.green, r.color.blue};
				end
				5'hc: begin
					readdata_o[15:0] <= r.frame_size.width;
					readdata_o[31:16] <= r.frame_size.height;
				end
				5'h10: begin
					readdata_o[9:0] <= r.sync_length_h.bpw;
					readdata_o[19:10] <= r.sync_length_h.fpw;
					readdata_o[29:20] <= r.sync_length_h.sw;
				end
				5'h14: begin
					readdata_o[9:0] <= r.sync_length_v.bpw;
					readdata_o[19:10] <= r.sync_length_v.fpw;
					readdata_o[29:20] <= r.sync_length_v.sw;
				end
				5'h18: begin
					readdata_o[0] <= r.status.frame_underflow;
					readdata_o[1] <= r.status.frame_overflow;
				end
				// 5'h10: begin
					// readdata_o[31] <= r.ctl.device_reset; 
					// readdata_o[8] <= rbuf.ctl.en;
					// readdata_o[9] <= rbuf.ctl.source;
					// readdata_o[30] <= rbuf.ctl.frame_buffer_enable;
					// readdata_o[29] <= rbuf.ctl.bicubic_enable;
					// readdata_o[28] <= rbuf.ctl.tee_en;
				// end
				// 5'h11: begin
					// readdata_o[23:0] <= {rbuf.background.red, rbuf.background.green, rbuf.background.blue};
				// end
				// 5'h12: begin
					// readdata_o[23:0] <= {rbuf.color.red, rbuf.color.green, rbuf.color.blue};
				// end
				// 5'h13: begin
					// readdata_o[15:0] <= rbuf.frame_size.width;
					// readdata_o[31:16] <= rbuf.frame_size.height;
				// end
				// 5'h14: begin
					// readdata_o[9:0] <= rbuf.sync_length_h.bpw;
					// readdata_o[19:10] <= rbuf.sync_length_h.fpw;
					// readdata_o[29:20] <= rbuf.sync_length_h.sw;
				// end
				// 5'h15: begin
					// readdata_o[9:0] <= rbuf.sync_length_v.bpw;
					// readdata_o[19:10] <= rbuf.sync_length_v.fpw;
					// readdata_o[29:20] <= rbuf.sync_length_v.sw;
				// end
				// 5'h16: begin
					// readdata_o[0] <= r.status.frame_underflow;
					// readdata_o[1] <= r.status.frame_overflow;
				// end
			endcase
		end
		if (write_i & ~release_transaction) begin
			case (address_i[4:0])
				5'h0: begin
					// r.ctl.device_reset <= 1;
					r.ctl.frame_buffer_enable <= writedata_i[30];
					r.ctl.bicubic_enable <= writedata_i[29];
					r.ctl.tee_en <= writedata_i[28];
					r.ctl.en <= writedata_i[8];
					r.ctl.source <= writedata_i[9];
					// if (writedata_i[4]) begin
						// r.ctl.frame_buffer_enable <= rbuf.ctl.frame_buffer_enable;
						// r.ctl.bicubic_enable <= rbuf.ctl.bicubic_enable;
						// r.ctl.tee_en <= rbuf.ctl.tee_en;
						// r.ctl.en <= rbuf.ctl.en;
						// r.ctl.source <= rbuf.ctl.source;
						// r.background <= rbuf.background;
						// r.color <= rbuf.color;
						// r.frame_size <= rbuf.frame_size;
						// r.sync_length_h <= rbuf.sync_length_h;
						// r.sync_length_v <= rbuf.sync_length_v;
					// end
					if (writedata_i[0]) begin
						// r.ctl.device_reset <= 0;
						r.ctl.frame_buffer_enable <= 0;
						r.ctl.bicubic_enable <= 0;
						r.ctl.tee_en <= 0;
						r.ctl.en <= 0;
						r.ctl.source <= 0;
						r.ctl.load_ctrls <= 0;
						r.ctl.clr <= 0;
						r.background.red <= 8'h00;
						r.background.green <= 8'h00;
						r.background.blue <= 8'h00;
						r.color.red <= 8'hff;
						r.color.green <= 8'hff;
						r.color.blue <= 8'hff;
						r.frame_size.width <= 16'd479;
						r.frame_size.height <= 16'd696;
						r.sync_length_h.bpw <= 10'd103;
						r.sync_length_h.fpw <= 10'd88;
						r.sync_length_h.sw <= 10'd15;
						r.sync_length_v.bpw <= 10'd198;
						r.sync_length_v.fpw <= 10'd178;
						r.sync_length_v.sw <= 10'd20;
					end
				end
				5'h4: begin
					r.background <= writedata_i[23:0];
				end
				5'h8: begin
					r.color <= writedata_i[23:0];
				end
				5'hc: begin
					r.frame_size.width <= writedata_i[15:0];
					r.frame_size.height <= writedata_i[31:16];
				end
				5'h10: begin
					r.sync_length_h.bpw <= writedata_i[9:0];
					r.sync_length_h.fpw <= writedata_i[19:10];
					r.sync_length_h.sw <= writedata_i[29:20];
				end
				5'h14: begin
					r.sync_length_v.bpw <= writedata_i[9:0];
					r.sync_length_v.fpw <= writedata_i[19:10];
					r.sync_length_v.sw <= writedata_i[29:20];
				end
			endcase
		end
		// if (r.ctl.device_reset & ~rbuf.ctl.device_reset) anrst <= 32'd0;
		// rbuf.ctl.device_reset <= r.ctl.device_reset;
		// if (anrst[30] & ~anrst[31]) r.ctl.device_reset <= 0;
	end
end

// -----------------------------------------------

assign frame_buffer_en_o = r.ctl.frame_buffer_enable;
assign bicubic_en_o = r.ctl.bicubic_enable;
assign switch_off_o = ~r.ctl.en;
assign source_o = r.ctl.source;
assign bg_o[2] = r.background.red;
assign bg_o[1] = r.background.green;
assign bg_o[0] = r.background.blue;
assign cl_o[2] = r.color.red;
assign cl_o[1] = r.color.green;
assign cl_o[0] = r.color.blue;
assign width_o = r.frame_size.width[9:0];
assign height_o = r.frame_size.height[9:0];
assign vbp_o = r.sync_length_v.bpw;
assign vfp_o = r.sync_length_v.fpw;
assign vsw_o = r.sync_length_v.sw;
assign hbp_o = r.sync_length_h.bpw;
assign hfp_o = r.sync_length_h.fpw;
assign hsw_o = r.sync_length_h.sw;
	
endmodule
