`timescale 1 ns / 1 ps

module lcd_bridge_mcu_top_test ();

	reg         clk_clk = 1'b0;
	wire        lcd_bridge_0_pixclock_clk;       //    lcd_bridge_0_pixclock.clk
	wire [15:0] lcd_bridge_0_rgb_rgb_o;          //         lcd_bridge_0_rgb.rgb_o
	wire        lcd_bridge_0_rgb_vsync_o;        //                         .vsync_o
	wire        lcd_bridge_0_rgb_hsync_o;        //                         .hsync_o
	wire        lcd_bridge_0_rgb_enable_o;       //                         .enable_o

	wire lcd_bridge_0_mcu_itf_dcx, lcd_bridge_0_mcu_itf_csx, lcd_bridge_0_mcu_itf_wrx, lcd_bridge_0_select_sel;
	wire ss_export_local, sck_export_local, dc_export_local;
	
	always #10 clk_clk = ~clk_clk;
	
	// assign ss_export = lcd_bridge_0_select_sel ? lcd_bridge_0_mcu_itf_csx : ss_export_local;
	// assign sck_export = lcd_bridge_0_select_sel ? lcd_bridge_0_mcu_itf_wrx : sck_export_local;
	// assign dc_export = lcd_bridge_0_select_sel ? lcd_bridge_0_mcu_itf_dcx : dc_export_local;

	wire pll_clock;
	reg [3:0] rst_cnt = 4'd0;
	always @(posedge clk_clk)
		if (!rst_cnt[3]) rst_cnt <= rst_cnt + 1;
		
	wire [1:0]  wb_pix;
	wire wb_vsync;
	wire wb_hsync;
	wire wb_clock;
	wire locked;

	pll50to20 pll50to20_inst(
		.areset(~rst_cnt[3]),
		.inclk0(clk_clk),
		.c0(pll_clock),
		.locked(locked));
	
	GB_sync_gen GB_sync_gen_inst(
		  .clock20_i(pll_clock)
		, .nrst_i(locked)
		, .gb_clock_o(wb_clock)
		, .gb_vsync_o(wb_vsync)
		, .gb_hsync_o(wb_hsync)
	);
	
	GB_data_gen GB_data_gen_inst(
		  .clock_i(wb_clock)
		, .vsync_i(wb_vsync)
		, .hsync_i(wb_hsync)
		, .pix_o(wb_pix)
	);
	
	lcd_bridge_top_testwrapper dut(
		  .clk_i(clk_clk)
		, .clk20_i(pll_clock)
		, .nrst_i(locked)
		, .pix_i(wb_pix)
		, .vsync_i(wb_vsync)
		, .hsync_i(wb_hsync)
		, .wb_clk_i(wb_clock)
	// -------------------------------
		, .vsync_o(lcd_bridge_0_rgb_vsync_o)
		, .hsync_o(lcd_bridge_0_rgb_hsync_o)
		, .rgb_o(lcd_bridge_0_rgb_rgb_o)
		, .enable_o(lcd_bridge_0_rgb_enable_o)
		, .pixclock_o(lcd_bridge_0_pixclock_clk)
/*		, .dcx_o()
		, .csx_o()
		, .wrx_o()
		, .vsync_interface_enable_o()
		, .pll_clock_out(pll_clock)*/
	);
	
endmodule
