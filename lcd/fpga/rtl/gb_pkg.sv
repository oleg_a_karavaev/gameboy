`ifndef GB_PKG
`define GB_PKG
package gb_pkg;

	typedef struct packed {
		logic [1:0] pixel;
		logic sop;
		logic eop;
		logic valid;
	} gameboy_frame_t;
	
	typedef struct packed {
		logic [31:0] pixels;
		logic sop;
		logic eop;
		logic valid;
	} packed16_gameboy_frame_t;
	
endpackage
`endif // COMMUNICATOR_PKG
