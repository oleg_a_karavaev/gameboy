`include "gb_pkg.sv"
import gb_pkg::*;
//`define CLK_SYNC
module gameboy_video_receiver (
	  input clock_i
	, input vsync_i
	, input hsync_i
	, input clk_i
	, output gb_pscl_o
	, input [1:0] pixel_i
	, output gameboy_frame_t gameboy_frame_o
);
	
	gameboy_frame_t gameboy_frame = '{2'd0, 1'b0, 1'b0, 1'b0};
	
	assign gameboy_frame_o = gameboy_frame;
	
	logic [7:0] linecnt, pixcnt;
	
	logic [3:0] clock_sync = 4'd0;
	logic [3:0] [1:0] pixel_sync = 8'd0;
	logic [3:0] hsync_sync = 4'd0;
	logic [3:0] vsync_sync = 4'd0;
	
	logic valid = 1'b0;
	
	always @(posedge clk_i) begin
		clock_sync <= {clock_sync[2:0], clock_i};
		pixel_sync <= {pixel_sync[2:0], pixel_i};
		hsync_sync <= {hsync_sync[2:0], hsync_i};
		vsync_sync <= {vsync_sync[2:0], vsync_i};
	end
`ifdef CLK_SYNC
	assign gb_pscl_o = clock_sync[2] & ~clock_sync[3];
	always @(posedge clk_i) begin
		if (gb_pscl_o) begin
			gameboy_frame.sop <= 0;
			gameboy_frame.eop <= 0;
			gameboy_frame.pixel <= pixel_sync[2];
			pixcnt <= pixcnt + 1;
			if (pixcnt == 158) begin
				if (linecnt == 143) gameboy_frame.eop <= 1'b1;
			end
			if (pixcnt == 159) gameboy_frame.valid <= 1'b0;
		end
		if (hsync_sync[3] & ~hsync_sync[2]) begin
			linecnt <= linecnt + 1;
			gameboy_frame.valid <= 1'b1;
			pixcnt <= 0;
			if (vsync_sync[2]) begin
				gameboy_frame.sop <= 1'b1;
				linecnt <= 0;
			end
		end
	end
`else
	always @(negedge clock_i) gameboy_frame.valid <= valid;
	assign gb_pscl_o = 1'b1;
	always @(posedge clock_i) begin
		gameboy_frame.sop <= 0;
		gameboy_frame.eop <= 0;
		pixcnt <= pixcnt + 1;
		gameboy_frame.pixel <= pixel_i;
		if (hsync_i) begin
			linecnt <= linecnt + 1;
			valid <= 1'b1;
			pixcnt <= 0;
			if (vsync_i) begin
				gameboy_frame.sop <= 1'b1;
				linecnt <= 0;
			end
		end
		if (pixcnt == 158) begin
			if (linecnt == 143) gameboy_frame.eop <= 1'b1;
		end
		if (pixcnt == 159) valid <= 1'b0;
	end
`endif	

endmodule
	