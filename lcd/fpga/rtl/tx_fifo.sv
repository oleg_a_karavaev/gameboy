/*
Модель FIFO. Параметризацется размером и типом описания: behav - поведенческое описание, иначе - на основе IP Core
На основе IP Core не стал делать, т.к. вполне годится и behav.
IP Core может использоваться только внутри этого модуля, т.к. поведение имеет некоторые особенности
*/

module tx_fifo #(
	  parameter size = 256
	, parameter logic behav = 0
	, parameter dwidth = 4
)(
	  input wclk_i     // - клок записи
	, input we_i       // - сигнал записи
	, input [dwidth-1:0] d_i  // - вх данные
	, input rclk_i     // - клок чтения
	, input re_i       // - чигнал чтения
	, output [dwidth-1:0] q_o // - вых данные
	, output empty_o   // - флаг "пусто"
	, output reg [$clog2(size)-1:0] rdusedw
);

generate
if (behav) begin
	logic [$clog2(size):0] waddr = 0; //начальные значения для удобства симуляции. По логике применения перед данными будет sop и адреса сбросятся
	logic [$clog2(size):0] raddr = 0;
	
	logic [dwidth-1:0] q;
	assign q_o = q;
	logic [dwidth-1:0] mem [0:size-1];
	logic [$clog2(size):0] waddr_next;
	logic [$clog2(size):0] raddr_next;
	logic [$clog2(size):0] wptr_gray = 0;
	logic [$clog2(size):0] rptr_gray = 0;
	logic [$clog2(size):0] wptr_gray_rclk = 0;
	logic [$clog2(size):0] wptr_rclk = 0;
	logic [$clog2(size):0] wptr_rclk_net;
	logic [$clog2(size):0] rptr_gray_wclk = 0;
	
	assign waddr_next = waddr + 1;
	assign raddr_next = raddr + 1;
	
	assign empty_o = rptr_gray == wptr_gray_rclk;

	always @(posedge wclk_i) if (we_i) mem[waddr[$clog2(size)-1:0]] <= d_i;
	
	always @(posedge rclk_i) if (re_i) q <= mem[raddr[$clog2(size)-1:0]];
	
	//	управление адресами
	always @(posedge wclk_i) begin
		if (we_i) begin
			waddr <= waddr_next;
			wptr_gray <= (waddr_next >> 1) ^ waddr_next;
		end
		rptr_gray_wclk <= rptr_gray;
	end
	
	always @(*) for (int i=0; i<$clog2(size)+1; i++)
		wptr_rclk_net[i] = ^(wptr_gray_rclk >> i);
		
	always @(posedge rclk_i) begin
		if (re_i) begin
			raddr <= raddr_next;
			rptr_gray <= (raddr_next >> 1) ^ raddr_next;
		end
		wptr_rclk <= wptr_rclk_net;
		rdusedw <= wptr_rclk - raddr;
		wptr_gray_rclk <= wptr_gray;
	end
end
endgenerate

endmodule