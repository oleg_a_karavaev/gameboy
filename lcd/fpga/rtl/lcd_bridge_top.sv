// TODO: connect Video data to the RGB interface
`include "gb_pkg.sv"
import gb_pkg::*;

module lcd_bridge_top (
	  input clk_i
	, input clk20_i
	, input nrst_i
	, input [1:0] pix_i
	, input vsync_i
	, input hsync_i
	, input wb_clk_i
//	, output valid_o
//	, output [4:0] pix_r_o
//	, output [4:0] pix_g_o
//	, output [4:0] pix_b_o
//	, output vsync_o
//	, output hsync_o
//	, output pixclk_o
// Avalon-MM
	, input [4:0] address_i
	, input [31:0] writedata_i
	, input read_i
	, input write_i
	, output [31:0] readdata_o
	, output waitrequest_o
	, output [1:0] response_o
// -------------------------------
	, output vsync_o
	, output hsync_o
	, output [15:0] rgb_o
	, output enable_o
	, output pixclock_o
/*	, output dcx_o
	, output csx_o
	, output wrx_o
	, output vsync_interface_enable_o*/
);

	logic bicubic_en, frame_buffer_en;
	logic [2:0] [7:0] bg, cl;
	logic valid, pixclk, vsync, hsync;
	logic [15:0] rst_reg = 16'd0;
	logic source;
	
	logic [23:0] rgb;
	logic [7:0] r, g, b;
	
	always @(negedge nrst_i or posedge clk_i)
		if (~nrst_i)
			rst_reg <= 16'd0;
		else
			rst_reg <= {rst_reg[14:0], 1'b1};
	
	logic nrst_common, anrst;
	logic clk, locked;
	assign nrst_common = rst_reg[3] & anrst;
	//assign periph_nrst_o = nrst_common;
//	wire [8:0] vsync_shift, hsync_shift;
	
	
	logic [7:0] pix_r, pix_g, pix_b;
	
	logic [14:0] line_length;
	logic hsync_flag;
	logic vsync_flag;
	
//	assign pix_r_o = pix_r[7:3];
//	assign pix_g_o = pix_g[7:3];
//	assign pix_b_o = pix_b[7:3];
	
	assign clk = clk20_i;
		
	logic rgb_req, rgb_ready, rgb_valid;
	logic rgb_req_ff = 1'b0;

	
		
	// frame_processing /*#(
		  // .total_width(489)
		// , .total_height(329)
		// , .vsync_start(483)
		// , .vsync_width(3)
		// , .hsync_start(323)
		// , .hsync_width(3)
	// ) */frame_processing_inst (
		  // .clk_i(clk)
		// , .nrst_i(nrst_common)
		// , .bicubic_en_i(bicubic_en)
		// , .frame_buffer_en_i(frame_buffer_en)
		// , .pix_i(pix_i)
		// , .vsync_i(vsync_i)
		// , .hsync_i(hsync_i)
		// , .wb_clk_i(wb_clk_i)
		// , .valid_o(valid)
		// , .pix_r_o(pix_r)
		// , .pix_g_o(pix_g)
		// , .pix_b_o(pix_b)
		// , .rgb_ready_o(rgb_ready)
		// , .rgb_valid_o(rgb_valid)
		// , .rgb_req_i(rgb_req)
		// , .line_length_i(line_length)
		// , .hsync_flag_i(hsync_flag)
		// , .vsync_flag_i(vsync_flag)
		// , .pixclk_o(pixclk)
		// , .bg_i(bg)
		// , .cl_i(cl)
	// );
	packed16_gameboy_frame_t packed16_gameboy_frame;
	gameboy_frame_t gameboy_frame;
	logic [1:0] wb_pixels;
	
	logic [31:0] ram_wdata;
	logic [10:0] ram_waddr;
	logic [2:0]  ram_we;
	logic [2:0] [31:0] ram_rdata_net;
	logic [31:0] ram_rdata;
	logic [10:0] ram_raddr;
	logic  [2:0] ram_re, ram_re_ff;

	logic [3:0] rst20;
	
	logic hactive, vactive, read_pixel_pscl, repeat_line_enable;
	logic [31:0] pixels_bc;
	logic req_field;
	logic repeat_request_enable;
	logic switch_to_next_line_en;
	logic to_next_line_enable_o;
	logic frame_start;
	frame_buffer_control frame_buffer_control_inst(
		  /*input */                         .clk_i(clk)
		, /*input packed16_gameboy_frame_t*/ .packed16_gameboy_frame_i(packed16_gameboy_frame)
		, /*output [31:0]*/                  .pixels_o(wb_pixels)
		, /*output [31:0]*/                  .pixels_bc_o(pixels_bc)
		, /*input */                         .hactive_i(hactive)
		, /*input */                         .vactive_i(vactive)
		, /*input */                         .pscl_i(read_pixel_pscl)
		, /*output*/                         .buffer_ready_o(rgb_ready)
		, /*input */                         .repeat_line_enable_i(repeat_line_enable)
		,                                    .pixels_valid_o(rgb_valid)
		, /*input */                         .bicubic_mode_en_i(bicubic_en)
		, /*input */                         .switch_to_next_line_en_i(switch_to_next_line_en)
		, /*input */                         .req_field_i(req_field)
		, .bc_frame_start_i(frame_start)
		, .repeat_request_enable_i(repeat_request_enable)
		// RAM itf
		, /*output [31:0]*/                  .ram_wdata_o(ram_wdata)
		, /*output [10:0]*/                  .ram_waddr_o(ram_waddr)
		, /*output [2:0] */                  .ram_we_o(ram_we)
		, /*input  [31:0]*/                  .ram_rdata_i(ram_rdata)
		, /*output [10:0]*/                  .ram_raddr_o(ram_raddr)
		, /*input  [2:0] */                  .ram_re_o(ram_re)
		, .bc_to_next_line_enable_i(to_next_line_enable)
	);	
	
	logic [2:0] [7:0] pixels_color_bc;
	logic [2:0] [7:0] pixels_color_bc_ff0 = 0, pixels_color_bc_ff1 = 0; 
	logic pixels_color_valid;

	
	logic frame_start_en = 1'b0;
	logic [1:0] vsync_sync = 2'b00;
	
	logic bicubic_req;
	
	always @(posedge clk) rgb_req_ff <= bicubic_req;
	
	always @(posedge clk) begin
		vsync_sync <= {vsync_sync[0], vsync_o};
		if (vsync_sync[0] & ~vsync_sync[1]) frame_start_en <= 1'b1;
		if (frame_start_en && frame_start) frame_start_en <= 1'b0;
	end

	// always @(negedge clk) begin
	// 	pixels_color_bc_ff0 <= pixels_color_bc;
	// 	pixels_color_bc_ff1 <= pixels_color_bc_ff0;
	// end

	assign frame_start = frame_start_en & bicubic_req & ~rgb_req_ff;

	bicubic_data_manager bicubic_data_manager_inst (
		  /*input */.nrst_i(rst20[3])
		, /*input */.clk_i(clk)
		, .en_i(bicubic_en)
		, /*input [31:0] */.pixels_i(pixels_bc)
		, /*input */.pixels_valid_i(rgb_valid)
		, /*input [7:0] */.bg_r_i(bg[2])
		, /*input [7:0] */.bg_g_i(bg[1])
		, /*input [7:0] */.bg_b_i(bg[0])
		, /*input [7:0] */.c_r_i(cl[2])
		, /*input [7:0] */.c_g_i(cl[1])
		, /*input [7:0] */.c_b_i(cl[0])
		, /*input */.frame_start_i(frame_start)
		, /*input */.line_start_i(bicubic_req & ~rgb_req_ff)
		, /*output */.switch_to_next_line_en_o(switch_to_next_line_en)
		, /*output */.field_request_o(req_field)
		, /*output [2:0] [7:0] */.pixels_color_o(pixels_color_bc)
		, /*output */.pixels_color_valid_o(pixels_color_valid)
		, .repeat_request_enable_o(repeat_request_enable)
		, .to_next_line_enable_o(to_next_line_enable)
	);

	always @(posedge clk)
		ram_re_ff <= ram_re;
		
	always @(*)
		if (ram_re_ff[0])
			ram_rdata = ram_rdata_net[0];
		else if (ram_re_ff[1])
			ram_rdata <= ram_rdata_net[1];
		else if (ram_re_ff[2])
			ram_rdata <= ram_rdata_net[2];
		else
			ram_rdata <= 32'd0;
	
	logic gb_pscl;
	
	dpram1440x32 buffer_0 (
		  .clock(clk)
		, .data(ram_wdata)
		, .rdaddress(ram_raddr)
		, .rden(ram_re[0])
		, .wraddress(ram_waddr)
		, .wren(ram_we[0])
		, .q(ram_rdata_net[0])
	);
	dpram1440x32 buffer_1 (
		  .clock(clk)
		, .data(ram_wdata)
		, .rdaddress(ram_raddr)
		, .rden(ram_re[1])
		, .wraddress(ram_waddr)
		, .wren(ram_we[1])
		, .q(ram_rdata_net[1])
	);
	dpram1440x32 buffer_2 (
		  .clock(clk)
		, .data(ram_wdata)
		, .rdaddress(ram_raddr)
		, .rden(ram_re[2])
		, .wraddress(ram_waddr)
		, .wren(ram_we[2])
		, .q(ram_rdata_net[2])
	);
	
	

	always @(posedge clk or negedge nrst_i)
		if (~nrst_i)
			rst20 <= 0;
		else
			rst20 <= {rst20[2:0], 1'b1};

	cmixer cmixer_inst(
		  /*input */       .clk_i(clk)
		, /*input */       .nrst_i(rst20[3])
		, /*input [7:0] */ .bg_r_i(bg[2])
		, /*input [7:0] */ .bg_g_i(bg[1])
		, /*input [7:0] */ .bg_b_i(bg[0])
		, /*input [7:0] */ .c_r_i(cl[2])
		, /*input [7:0] */ .c_g_i(cl[1])
		, /*input [7:0] */ .c_b_i(cl[0])
		, /*input [1:0] */ .p_i(wb_pixels)
		, /*output [7:0]*/ .p_r_o(pix_r)
		, /*output [7:0]*/ .p_g_o(pix_g)
		, /*output [7:0]*/ .p_b_o(pix_b)
	);
	
	gameboyframe_to_framebuffer_bridge gameboyframe_to_framebuffer_bridge_inst(
		  /*input */                          .clk_i(clk)
		,                                     .gb_pscl_i(gb_pscl)
		, /*input */                          .gb_clock_i(wb_clk_i) //clk)
		, /*input gameboy_frame_t*/           .gameboy_frame_i(gameboy_frame)
		, /*output packed16_gameboy_frame_t*/ .packed16_gameboy_frame_o(packed16_gameboy_frame)
	);

	gameboy_video_receiver gameboy_video_receiver_inst(
		  /*input */                 .clock_i(wb_clk_i)
		, /*input */                 .vsync_i(vsync_i)
		, /*input */                 .hsync_i(hsync_i)
		, /*input [1:0]*/            .pixel_i(pix_i)
		, /*output gameboy_frame_t*/ .gameboy_frame_o(gameboy_frame)
		, .clk_i(clk_i)
		, .gb_pscl_o(gb_pscl)
	);
	
	logic switch_off;
	logic dbi_txbuf_wrclk, dbi_txbuf_wr, dbi_cmd_wr;
	logic [1:0] input_data_size;
	logic [9:0] width, height, vbp, vfp, vsw, hbp, hfp, hsw;
	logic [4:0] data_latency;
	assign data_latency = bicubic_en ? 5'd12 : 5'd0;

	logic [23:0] rgb_data;
	assign rgb_data = bicubic_en ? pixels_color_bc : {pix_r, pix_g, pix_b};
	
	rgb_itf #(.data_latency(3)) rgb_itf_inst (
		  /*input*/         .clk_i(clk)
		, /*input*/         .nrst_i(rst20[3])
		, /*input*/         .frame_ready_i(rgb_ready)
		, /*input*/         .switch_off_i(switch_off)
		, /*input*/         .source_i(source)
		, /*input*/         .valid_i(rgb_valid)
		, /*output*/        .req_o(rgb_req)
		, .bicubic_req_o(bicubic_req)
		, /*input [9:0]*/   .width_i(width)
		, /*input [9:0]*/   .height_i(height)
		, /*input [9:0]*/   .vbp_i(vbp)
		, /*input [9:0]*/   .vfp_i(vfp)
		, /*input [9:0]*/   .vsw_i(vsw)
		, /*input [9:0]*/   .hbp_i(hbp)
		, /*input [9:0]*/   .hfp_i(hfp)
		, /*input [9:0]*/   .hsw_i(hsw)
		// , /*input [9:0]*/   .data_latency_i(data_latency)
		,                   .line_length_o(line_length)
		,                   .hsync_flag_o(hsync_flag)
		,                   .vsync_flag_o(vsync_flag)
		,                   .hactive_o(hactive)
		,                   .vactive_o(vactive)
		,                   .read_pixel_pscl_o(read_pixel_pscl)
		,                   .repeat_line_enable_o(repeat_line_enable)
		, /*input [23:0]*/  .rgb_i(rgb_data)
		, /*output*/        .vsync_o(vsync_o)
		, /*output*/        .hsync_o(hsync_o)
		, /*output [23:0]*/ .rgb_o(rgb)
		, /*output*/        .enable_o(enable_o)
		, .dcx_o() //dcx_o)
		, .csx_o() //csx_o)
		, .wrx_o() //wrx_o)
		, .vsync_interface_enable_o() //vsync_interface_enable_o)
	);
	
	assign r = rgb[23:16];
	assign g = rgb[15:8];
	assign b = rgb[7:0];
	assign pixclock_o = clk; //pixclk;
	assign rgb_o = {r[7:3], g[7:2], b[7:3]};

	
	user_itf user_itf_inst (
		  .clk_i(clk)
		, .nrst_i(rst20[3])
		, .address_i(address_i)
		, .writedata_i(writedata_i)
		, .read_i(read_i)
		, .write_i(write_i)
		, .readdata_o(readdata_o)
		, .waitrequest_o(waitrequest_o)
		, .response_o(response_o)
		// internal ctrl
		, .anrst_o(anrst)
		, .frame_buffer_en_o(frame_buffer_en)
		, .bicubic_en_o(bicubic_en)
		, .bg_o(bg)
		, .cl_o(cl)
		, .width_o(width)
		, .height_o(height)
		, .vbp_o(vbp)
		, .vfp_o(vfp)
		, .vsw_o(vsw)
		, .hbp_o(hbp)
		, .hfp_o(hfp)
		, .hsw_o(hsw)
//		, .data_latency_o(data_latency)
		, .switch_off_o(switch_off)
		, .source_o(source)
		, .frame_overflow_i(1'b0)
		, .frame_underflow_i(1'b0)
	);
	

endmodule
