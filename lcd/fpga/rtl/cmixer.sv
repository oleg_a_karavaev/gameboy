module cmixer (
	  input clk_i
	, input nrst_i
	, input [7:0] bg_r_i
	, input [7:0] bg_g_i
	, input [7:0] bg_b_i
	, input [7:0] c_r_i
	, input [7:0] c_g_i
	, input [7:0] c_b_i
	, input [1:0] p_i
	, output [7:0] p_r_o
	, output [7:0] p_g_o
	, output [7:0] p_b_o
);

	reg [8:0] d_r, d_g, d_b;
	reg [8:0] d_r_div2, d_g_div2, d_b_div2, d_r_div4, d_g_div4, d_b_div4;
	reg [8:0] p_r, p_g, p_b;
	reg [7:0] c_r_ff0, c_r_ff1, c_g_ff0, c_g_ff1, c_b_ff0, c_b_ff1;
	
	always @(negedge clk_i or negedge nrst_i)
		if (~nrst_i) begin
			d_r <= 9'd0;
			d_g <= 9'd0;
			d_b <= 9'd0;
			d_r_div2 <= 9'd0;
			d_g_div2 <= 9'd0;
			d_b_div2 <= 9'd0;
			d_r_div4 <= 9'd0;
			d_g_div4 <= 9'd0;
			d_b_div4 <= 9'd0;
			p_r <= 9'd0;
			p_g <= 9'd0;
			p_b <= 9'd0;
			c_r_ff0 <= 8'd0;
			c_r_ff1 <= 8'd0;
			c_g_ff0 <= 8'd0;
			c_g_ff1 <= 8'd0;
			c_b_ff0 <= 8'd0;
			c_b_ff1 <= 8'd0;
		end
		else begin
			c_r_ff0 <= c_r_i;
			c_r_ff1 <= c_r_ff0;
			c_g_ff0 <= c_g_i;
			c_g_ff1 <= c_g_ff0;
			c_b_ff0 <= c_b_i;
			c_b_ff1 <= c_b_ff0;
			d_r <= {1'b0, c_r_i} + ~{1'b0, bg_r_i} + 1;
			d_g <= {1'b0, c_g_i} + ~{1'b0, bg_g_i} + 1;
			d_b <= {1'b0, c_b_i} + ~{1'b0, bg_b_i} + 1;
			d_r_div2 <= {d_r[8], d_r[8:1]};
			d_g_div2 <= {d_g[8], d_g[8:1]};
			d_b_div2 <= {d_b[8], d_b[8:1]};
			d_r_div4 <= {d_r[8], d_r[8], d_r[8:2]};
			d_g_div4 <= {d_g[8], d_g[8], d_g[8:2]};
			d_b_div4 <= {d_b[8], d_b[8], d_b[8:2]};
			case (p_i)
				2'b00: begin
						p_r <= {1'b0, bg_r_i};
						p_g <= {1'b0, bg_g_i};
						p_b <= {1'b0, bg_b_i};
					end
				2'b01: begin
						p_r <= {1'b0, bg_r_i} + d_r_div4;
						p_g <= {1'b0, bg_g_i} + d_g_div4;
						p_b <= {1'b0, bg_b_i} + d_b_div4;
					end
				2'b10: begin
						p_r <= {1'b0, bg_r_i} + d_r_div4 + d_r_div2;
						p_g <= {1'b0, bg_g_i} + d_g_div4 + d_g_div2;
						p_b <= {1'b0, bg_b_i} + d_b_div4 + d_b_div2;
					end
				2'b11: begin
						p_r <= c_r_ff1;
						p_g <= c_g_ff1;
						p_b <= c_b_ff1;
					end
				default: begin
						p_r <= 9'd0;
						p_g <= 9'd0;
						p_b <= 9'd0;
					end
			endcase
		end
		
	assign p_r_o = p_r[7:0];
	assign p_g_o = p_g[7:0];
	assign p_b_o = p_b[7:0];

endmodule