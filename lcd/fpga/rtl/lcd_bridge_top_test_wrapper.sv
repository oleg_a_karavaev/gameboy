module lcd_bridge_top_testwrapper (
	  input clk_i
	, input clk20_i
	, input nrst_i
	, input [1:0] pix_i
	, input vsync_i
	, input hsync_i
	, input wb_clk_i
// -------------------------------
	, output vsync_o
	, output hsync_o
	, output [15:0] rgb_o
	, output enable_o
	, output pixclock_o
/*	, output dcx_o
	, output csx_o
	, output wrx_o
	, output vsync_interface_enable_o
	, output pll_clock_out*/
);

logic nrst;

initial begin
	@(posedge nrst_i);
	repeat (10) @(posedge clk_i);
	lcd_bridge_top_inst.user_itf_inst.r.ctl.device_reset = 0;
	lcd_bridge_top_inst.user_itf_inst.r.ctl.frame_buffer_enable = 0;
	lcd_bridge_top_inst.user_itf_inst.r.ctl.bicubic_enable = 1;
	lcd_bridge_top_inst.user_itf_inst.r.ctl.tee_en = 0;
	lcd_bridge_top_inst.user_itf_inst.r.ctl.en = 1;
	lcd_bridge_top_inst.user_itf_inst.r.ctl.load_ctrls = 0;
	lcd_bridge_top_inst.user_itf_inst.r.ctl.clr = 0;
	lcd_bridge_top_inst.user_itf_inst.r.ctl.source = 1;
	lcd_bridge_top_inst.user_itf_inst.r.background.red = 8'h00;
	lcd_bridge_top_inst.user_itf_inst.r.background.green = 8'h00;
	lcd_bridge_top_inst.user_itf_inst.r.background.blue = 8'hff; //8'h00;
	lcd_bridge_top_inst.user_itf_inst.r.color.red = 8'hff;
	lcd_bridge_top_inst.user_itf_inst.r.color.green = 8'hff;
	lcd_bridge_top_inst.user_itf_inst.r.color.blue = 8'h00; //8'hff;
	lcd_bridge_top_inst.user_itf_inst.r.frame_size.width = 16'd350;
	lcd_bridge_top_inst.user_itf_inst.r.frame_size.height = 16'd510;
	lcd_bridge_top_inst.user_itf_inst.r.sync_length_h.bpw = 10'd15;
	lcd_bridge_top_inst.user_itf_inst.r.sync_length_h.fpw = 10'd15;
	lcd_bridge_top_inst.user_itf_inst.r.sync_length_h.sw = 10'd10;
	lcd_bridge_top_inst.user_itf_inst.r.sync_length_v.bpw = 10'd15;
	lcd_bridge_top_inst.user_itf_inst.r.sync_length_v.fpw = 10'd15;
	lcd_bridge_top_inst.user_itf_inst.r.sync_length_v.sw = 10'd10;
end

lcd_bridge_top lcd_bridge_top_inst(
	  .clk_i(clk_i)
	, .clk20_i(clk20_i)
	, .nrst_i(nrst_i)
	, .pix_i(pix_i)
	, .vsync_i(vsync_i)
	, .hsync_i(hsync_i)
	, .wb_clk_i(wb_clk_i)
// Avalon-MM
	, .address_i(5'd0)
	, .writedata_i(32'd0)
	, .read_i(1'b0)
	, .write_i(1'b0)
	, .readdata_o()
	, .waitrequest_o()
	, .response_o()
// -------------------------------
	, .vsync_o(vsync_o)
	, .hsync_o(hsync_o)
	, .rgb_o(rgb_o)
	, .enable_o(enable_o)
	, .pixclock_o(pixclock_o)
/*	, .dcx_o(dcx_o)
	, .csx_o(csx_o)
	, .wrx_o(wrx_o)
	, .vsync_interface_enable_o(vsync_interface_enable_o)
	, .pll_clock_out(pll_clock_out)
	, .periph_nrst_o(nrst)*/
);

endmodule
